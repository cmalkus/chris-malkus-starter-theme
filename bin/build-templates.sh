#!/bin/bash

if  [[ $1 ]]; then

	if [ "$1"  != "-c" ]; then
		TEMPNAME=$1
	elif  [[ $2 ]]; then
		TEMPNAME=$2
		FLAG=true
	fi

	if [ -f "${BASH_SOURCE%/*}/../page-templates/$TEMPNAME-template.php" ] || [ -f "${BASH_SOURCE%/*}/../views/$TEMPNAME.twig" ] || [ -f "${BASH_SOURCE%/*}/../js/app/views/$TEMPNAME.js" ] || [ -f "${BASH_SOURCE%/*}/../css/views/_$TEMPNAME.scss" ]; then
		echo "File(s) with that name already exist. Aborting"
		exit 2
	fi

	echo 'Creating PHP Template...'
	if [ ! -d "${BASH_SOURCE%/*}/../page-templates" ]; then
		mkdir ${BASH_SOURCE%/*}/../page-templates
	fi
	CLEANNAME=`echo $TEMPNAME | tr '-' ' ' | tr '_' ' ' | perl -pe 's/./\u$&/'`
	cat <<EOF >${BASH_SOURCE%/*}/../page-templates/$TEMPNAME-template.php
<?php
/**
* Template Name: $CLEANNAME template
*
* Description:
*/

\$context = Timber::context();
\$post = new TimberPost();
\$context['post'] = \$post;
Timber::render( '$TEMPNAME.twig', \$context );
EOF

	echo 'Creating Twig Template...'
	if [ -f "${BASH_SOURCE%/*}/../views/page.twig" ]; then
		echo 'Copying page.twig...'
		cp ${BASH_SOURCE%/*}/../views/page.twig ${BASH_SOURCE%/*}/../views/$TEMPNAME.twig
	else
		echo 'No page.twig file found. Creating blank template...'
		touch ${BASH_SOURCE%/*}/../views/$TEMPNAME.twig
	fi
	echo 'Creating Javascript File...'
	if [ ! -d "${BASH_SOURCE%/*}/../js/app/views" ]; then
		mkdir ${BASH_SOURCE%/*}/../js/app/views
	fi
	cat <<EOF >${BASH_SOURCE%/*}/../js/app/views/$TEMPNAME.js
\$(document).ready(function(){

});
EOF

	echo 'Creating Advanced Custom Field Page Template File...'
	if [ ! -d "${BASH_SOURCE%/*}/../php/acf-views" ]; then
		mkdir ${BASH_SOURCE%/*}/../php/acf-views
	fi
	cat <<EOF >${BASH_SOURCE%/*}/../php/acf-views/$TEMPNAME-acf.php
<?php

EOF

	echo 'Creating SCSS File...'
	if [ ! -d "${BASH_SOURCE%/*}/../css/views" ]; then
		mkdir ${BASH_SOURCE%/*}/../css/views
	fi
	cat <<EOF >${BASH_SOURCE%/*}/../css/views/$TEMPNAME.scss
@import "css/mixins";
EOF
	exit 0
else
	echo 'File name required. Aborting.'
	exit 1
fi
