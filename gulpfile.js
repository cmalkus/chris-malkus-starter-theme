var pkg = require('./package.json'),
	banner = `/* Theme Name: <%= pkg.theme_name %>
		Theme URI:
		Author: <%= pkg.author %>
		Author URI: http://chrismalkus.com/
		Description: <%= pkg.description %>
		Version: <%= pkg.version %>
		License: <%= pkg.license %>
		License URI: <%= pkg.license_url %>
		Text Domain: <%= pkg.text_domain %>
		Domain Path: /languages */`,

	os = require('os'),
	gulp = require('gulp'),
	composer = require('gulp-composer'),
	sass = require('gulp-sass')(require('sass')),
	rtlcss = require('gulp-rtlcss'),
	cleancss = require('gulp-clean-css'),
	concat = require('gulp-concat'),
	uglify = require('gulp-uglify'),
	rename = require('gulp-rename'),
	header = require('gulp-header'),
	debug = require('gulp-debug'),
	gulpIf = require('gulp-if'),
	wpPot = require('gulp-wp-pot'),
	sourcemaps = require('gulp-sourcemaps'),
	babel = require('gulp-babel'),
	webpack = require('webpack-stream');

gulp.task('sass', function(){
	return gulp.src('*.scss')
		.pipe(debug())
		.pipe(sourcemaps.init())
		.pipe(sass.sync({
			outputStyle: 'compressed'
		}).on('error', sass.logError))
		.pipe(cleancss())
		.pipe(header(banner, {
			pkg: pkg
		}))
		.pipe(sourcemaps.write('./maps'))
		.pipe(gulp.dest('./'))
		.pipe(gulpIf('*.css', rtlcss()))
		.pipe(gulpIf('*.css', rename({
			suffix: '-rtl'
		})))
		.pipe(gulpIf('*.css', gulp.dest('./')));
});

gulp.task('sass-views', function(){
	return gulp.src('css/views/*.scss', {
		base: './'
	})
		.pipe(debug())
		.pipe(sourcemaps.init())
		.pipe(sass.sync({
			outputStyle: 'compressed'
		}).on('error', sass.logError))
		.pipe(cleancss())
		.pipe(sourcemaps.write('./maps'))
		.pipe(gulp.dest('.'));
});

gulp.task('scripts', function(){
	return gulp.src(['js/lib/**/*.js', 'js/app/functions.js', 'js/app/app.js', '!js/app/admin.js', '!js/**/~*', '!js/lib/ace/**/*.js', '!js/app/blocks/**/*.js'], {
		base: './'
	})
		.pipe(debug())
		.pipe(sourcemaps.init())
		.pipe(concat('main.js'))
		.pipe(rename('main.min.js'))
		.pipe(uglify())
		.pipe(sourcemaps.write('../maps'))
		.pipe(gulp.dest('js/'));
});

gulp.task('internationalize', function(done){
	gulp.src(['**/*.php', 'views/**/*.twig', 'js/app/**/*.js'])
		.pipe(wpPot({
			domain: pkg.text_domain,
			package: pkg.theme_name
		}))
		.pipe(gulp.dest('languages/'))
		.on('end', done);

	gulp.src(['**/*.php', 'views/**/*.twig', 'js/app/**/*.js'])
		.pipe(wpPot({
			domain: pkg.text_domain + '_admin',
			package: pkg.theme_name
		}))
		.pipe(gulp.dest('languages/'))
		.on('end', done);
});

gulp.task('composer', function(done){
	composer({
		'bin': 'bin/composer.phar'
	});
	done();
});

gulp.task('install-image-tools', function(done){
	switch (true){
		case (/^darwin/).test(os.platform()):
			gulp.src('./bin/darwin-*')
				.pipe(rename(function(opt){
					opt.basename = opt.basename.replace(/^darwin-/, '');
					return opt;
				}))
				.pipe(gulp.dest('./bin'));
			break;
		case (/^linux/).test(os.platform()):
			gulp.src('./bin/linux-*')
				.pipe(rename(function(opt){
					opt.basename = opt.basename.replace(/^linux-/, '');
					return opt;
				}))
				.pipe(gulp.dest('./bin'));
			break;
		case (/^win/).test(os.platform()):
			gulp.src('./bin/win-*')
				.pipe(rename(function(opt){
					opt.basename = opt.basename.replace(/^win-/, '');
					return opt;
				}))
				.pipe(gulp.dest('./bin'));
			break;
	}
	done();
});
// gulp.task('babel', () =>
// 	gulp.src(['js/app/blocks/src/**/*.js', 'js/app/blocks/src/**/*.jsx'])
// 		.pipe(babel({
// 			presets: ['@wordpress/babel-preset-default']
// 		}))
// 		.pipe(gulp.dest('js/app/blocks/dist'))
// );

// gulp.task('webpack', function(){
// 	gulp.src('js/app/blocks/dist/blocks.js')
// 		.pipe(webpack())
// 		.pipe(uglify())
// 		.pipe(gulp.dest('js/app/blocks/dist/'));
// });

gulp.task('default', gulp.series('composer', 'sass', 'sass-views', 'scripts', 'internationalize', 'install-image-tools'));

gulp.task('watch', function(done){
	gulp.watch(['*.scss', 'css/*.scss'], gulp.series('sass'));
	gulp.watch(['css/views/*.scss'], gulp.series('sass-views'));
	gulp.watch('js/*/*.js', gulp.series('scripts'));
	// gulp.watch(['js/app/blocks/src/**/*.js', 'js/app/blocks/src/**/*.jsx'], gulp.series('babel', 'webpack'));
	gulp.watch(['*.php', 'php/*.php', 'php/acf-views/*.php', 'views/**/*.twig', 'js/app/**/*.js'], gulp.series('internationalize'));
	done();
});
