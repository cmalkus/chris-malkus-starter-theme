/* global wp: true, acf: false, debounce: false */
import ResponsiveButtons from './ResponsiveButtons';
import { generateStyles, responsiveStyles } from './generateStyles';
import { mobile, tablet, desktop } from '@wordpress/icons';

acf.hooks.addAction('load', function(){
	wp.editor.responsiveViews = {
		'mobile': {
			name: 'mobile',
			icon: mobile,
			containerWidth: 480,
			mediaBreak: 0
		},
		'tablet': {
			name: 'tablet',
			icon: tablet,
			containerWidth: 768,
			mediaBreak: 481
		},
		'desktop': {
			name: 'desktop',
			icon: desktop,
			containerWidth: '100%',
			mediaBreak: 769
		}
	};

	wp.editor.responsiveMode = wp.editor.responsiveViews.desktop;
	const headerToolbar = document.querySelector('.edit-post-header-toolbar__left');
	const div = document.createElement('div');

	div.classList.add('edit-post-header-toolbar__center');
	if(headerToolbar){
		headerToolbar.parentNode.insertBefore(div, headerToolbar.nextSibling);
		wp.element.render(<ResponsiveButtons />, div);
	}
});

function addResponsiveStyle(settings, name) {
	if(!name.startsWith('core/')){
		return settings;
	}

	let changeDefaultAttribute = {
		attributes: {
			responsiveStyle: {
				type: "object",
			},
			blockId: {
				type: "string"
			}
		}
	};

	return lodash.merge({}, settings, changeDefaultAttribute);
}

function removeInlineStyle(element, blockType, attributes){
	lodash.assign(element.props, { style: null });

	return element;
}

function addDataAttr(props, blockType, attributes){
	return lodash.merge({}, props, {'data-block-id': attributes.blockId});
}

wp.hooks.addFilter(
	"blocks.registerBlockType",
	"finn/add-responsive-style",
	addResponsiveStyle
);

wp.hooks.addFilter(
	'blocks.getSaveElement',
	'finn/remove-inline-style',
	removeInlineStyle
);

wp.hooks.addFilter(
	'blocks.getSaveContent.extraProps',
	'finn/add-data-attr',
	addDataAttr
);

wp.domReady(() => {
	const unsubscribe = wp.data.subscribe(() => {
		const isSavingPost = wp.data.select('core/editor').isSavingPost();
		const isAutosavingPost = wp.data.select('core/editor').isAutosavingPost();
		const blocks = wp.data.select('core/block-editor').getBlocks();

		if (!isAutosavingPost && isSavingPost) {
			(function inheritStyles(blocks) {
				blocks.forEach(block => {
					block.attributes.responsiveStyle ??= {};

					block.attributes.responsiveStyle.tablet = lodash.merge({}, block.attributes.responsiveStyle.desktop, block.attributes.responsiveStyle.tablet ?? {});

					block.attributes.responsiveStyle.mobile = lodash.merge({}, block.attributes.responsiveStyle.tablet, block.attributes.responsiveStyle.mobile ?? {});

					if (block.innerBlocks.length) inheritStyles(block.innerBlocks);
				});
			})(blocks);

			unsubscribe();
		}
	});

	wp.data.subscribe(debounce(() => {
		const editor = wp.data.select('core/block-editor');
		const block = editor.getSelectedBlock();
		const blocks = editor.getBlocks();
		let styles = '';
		let stylesheet = document.getElementById('page-builder-styles');
		if (!block) return;

		let elem = document.getElementById(`block-${block.clientId}`);

		block.attributes.responsiveStyle ??= {};
		block.attributes.blockId = `block_${block.clientId.replaceAll(/-/g, '') }`;

		if (elem) elem.setAttribute('data-block-id', block.attributes.blockId);

		block.attributes.responsiveStyle[wp.editor.responsiveMode.name] = block.attributes.style;

		responsiveStyles = {
			mobile: '',
			tablet: '',
			desktop: ''
		};

		generateStyles(blocks);

		for(const view in responsiveStyles) {
			let width = wp.editor.responsiveViews[view].mediaBreak;

			if (view === 'mobile') {
				styles += `${responsiveStyles[view]}`;
			} else {
				styles += `@container content (min-width: ${width}px){${responsiveStyles[view]}}`;
			}
		};

		stylesheet.textContent = styles;
	}, 300));
});
