export var responsiveStyles = {
	mobile: '',
	tablet: '',
	desktop: ''
};

/**
 *
 * @param {*} blocks
 * @returns
 */
export const generateStyles = blocks => {
	if(!blocks.length) return;

	blocks.forEach((block) => {
		if(!block.attributes.responsiveStyle) return;
		let responsiveStyleObj = block.attributes.responsiveStyle;
		let blockId = block.attributes.blockId;

		for(const view in responsiveStyleObj){
			let rules = '';
			for(const value in responsiveStyleObj[view]){
				switch (value){
					case 'spacing':
						for(const prop in responsiveStyleObj[view][value]){
							for(const pos in responsiveStyleObj[view][value][prop]){
								rules += `${prop}-${pos}:${responsiveStyleObj[view][value][prop][pos]};`;
							}
						}
						break;
					case 'image':
						break;
					case 'color':
						for(const prop in responsiveStyleObj[view][value]){
							switch (prop){
								case 'background':
									rules += `background-color:${responsiveStyleObj[view][value][prop]};`;
									break;
								case 'text':
									rules += `color:${responsiveStyleObj[view][value][prop]};`;
									break;
								case 'gradient':
									rules += `background-image:${responsiveStyleObj[view][value][prop]};`;
									break;
								default:
									break;
							}
						}
						break;
					case 'typography':
						for(const prop in responsiveStyleObj[view][value]){
							let _prop = prop.split(/(?=[A-Z])/).join('-').toLowerCase();
							rules += `${_prop}:${responsiveStyleObj[view][value][prop]};`;
						}
						break;
					default:
						break;
				}
			}
			// console.log(rules);
			responsiveStyles[view] += `.block-editor-block-list__layout [data-block-id="${blockId}"]{${rules}}`;
		}

		generateStyles(block.innerBlocks);
	});
};
