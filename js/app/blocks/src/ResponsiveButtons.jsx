/* global wp: true */
import { useEffect, useState } from 'react';
import { ToolbarGroup, ToolbarButton } from '@wordpress/components';
import objectMap from './objectMap';
import { generateStyles, responsiveStyles } from './generateStyles';

/** */
const ResponsiveButtons = () => {
	const [responsiveMode, setResponsiveMode] = useState(wp.editor.responsiveMode);
	useEffect(() => {
		changeResponsiveMode(wp.editor.responsiveMode);
		setResponsiveMode(wp.editor.responsiveMode);
	});

	/** */
	const changeResponsiveMode = (view) => {
		let container = document.querySelector('.is-root-container.block-editor-block-list__layout');
		let blocks = wp.data.select('core/block-editor').getBlocks();
		let stylesheet = document.getElementById('page-builder-styles');
		let styles = '';

		wp.editor.responsiveMode = view;
		setResponsiveMode(wp.editor.responsiveMode);

		(function updateAtts(blocks){
			blocks.forEach(block => {
				let responsiveStyle = block.attributes.responsiveStyle && block.attributes.responsiveStyle[view.name] ? block.attributes.responsiveStyle[view.name] : block.attributes.style;
				let blockElem = document.querySelector(`[data-block-id=${block.attributes.blockId}]`) || document.getElementById(`block-${block.clientId}`);

				wp.data.dispatch('core/block-editor').updateBlockAttributes(block.clientId, { 'style': responsiveStyle, 'blockId': `block_${block.clientId.replaceAll(/-/g, '')}` });
				block.attributes.blockId = `block_${block.clientId.replaceAll(/-/g, '')}`;

				if(blockElem) blockElem.setAttribute('data-block-id', block.attributes.blockId);

				if(block.innerBlocks.length) updateAtts(block.innerBlocks);
			});
		})(blocks);

		responsiveStyles = {
			mobile: '',
			tablet: '',
			desktop: ''
		};

		generateStyles(blocks);

		for(const view in responsiveStyles){
			let width = wp.editor.responsiveViews[view].mediaBreak;

			if(view === 'mobile'){
				styles += `${responsiveStyles[view]}`;
			}else{
				styles += `@container content (min-width: ${width}px){${responsiveStyles[view]}}`;
			}
		};

		container.style.width = view.containerWidth;
		container.style.marginBottom = view.name !== 'desktop' ? '4em' : 0;
		container.style.marginTop = view.name !== 'desktop' ? '4em' : 0;
		stylesheet.textContent = styles;
	};

	return <ToolbarGroup>
		{objectMap(wp.editor.responsiveViews, view =>
			<ToolbarButton
				name={ view.name }
				icon={ view.icon }
				isPressed={ responsiveMode.name === view.name}
				onClick={ changeResponsiveMode.bind(this, view) }
			/>
		)}
	</ToolbarGroup>;
};

export default ResponsiveButtons;
