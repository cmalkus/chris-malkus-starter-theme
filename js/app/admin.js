/* globals ace: false, Uint8Array: false, siteUrl: true, templateUrl: true, google: false, __: false, qTranslateX: false, qTranslateConfig: false, wp: false, eventAddress: false, repeating: true, recurrence: true, noLocation: true, postId: false */

/**
 * Grabs the ULR parameters and returns an object
 * @param  {string} [query=window.location.search.substring(1)] Fully formatted URL, in case you dont want to use the current pages params
 * @return {object} URL params returned as an object
 */
function urlParams(query){
	var queryString = {},
		vars, pair, i, arr;

	query = query ? query.indexOf('?') === 0 ? query.split('?')[1] : query : window.location.search.substring(1);
	vars = query.split('&');

	for(i = 0; i < vars.length; i++){
		pair = vars[i].split('=');

		if(typeof queryString[pair[0]] === 'undefined'){
			queryString[pair[0]] = decodeURIComponent(pair[1]);
			// If second entry with this name
		}else if(typeof queryString[pair[0]] === 'string'){
			arr = [queryString[pair[0]], decodeURIComponent(pair[1])];
			queryString[pair[0]] = arr;
			// If third or later entry with this name
		}else{
			queryString[pair[0]].push(decodeURIComponent(pair[1]));
		}
	}

	return queryString;
}

/**
 * Debounces your function. ie waits for the the end of input before executing
 * @param  {Function} fn The function you want to debounce
 * @param  {Number} delay The amount of time before executing (in ms)
 * @return {Function} Returns the function with a timout
 */
function debounce(fn, delay){
	var timer = null;

	return function(){
		var context = this,
			args = arguments;

		clearTimeout(timer);
		timer = setTimeout(function(){
			fn.apply(context, args);
		}, delay);
	};
}

/**
 * Makes an ajax post to the import_google_cal function
 * @param  {obj} data  The form data
 * @param  {fn} _callback Callback function
 * @return {fn}           The callback
 */
function importGoogleCalendar(data, _callback){
	data = {
		action: 'import_google_cal'
	};

	jQuery.ajax({
		type: 'POST',
		url: siteUrl + '/wp-admin/admin-ajax.php',
		data: data,
		success: function(data){
			location.reload();
		}
	});
}

/**
 * Makes an ajax post to the download_submission_csv function
 * @param  {obj} data  The form data
 * @param  {fn} _callback Callback function
 * @return {fn}           The callback
 */
function downloadSubmissionCSV(data, _callback){
	data = {
		action: 'download_submission_csv',
		data: urlParams()
	};

	jQuery.ajax({
		type: 'POST',
		url: siteUrl + '/wp-admin/admin-ajax.php',
		dataType: 'text',
		mimeType: 'text/plain; charset=x-user-defined',
		data: data,
		success: function(data, status, xhr){
			var content = '',
				a = jQuery("<a style='display: none;'/>"),
				file, url, bytes, ni;

			if(xhr.getResponseHeader('content-type') === 'application/zip'){
				for(ni = 0; ni < data.length; ++ni){
					content += String.fromCharCode(data.charCodeAt(ni) & 0xFF);
				}

				bytes = new Uint8Array(content.length);
				for(ni = 0; ni < content.length; ++ni){
					bytes[ni] = content.charCodeAt(ni);
				}

				file = new Blob([bytes], {type: xhr.getResponseHeader('content-type'), encoding: xhr.getResponseHeader('content-encoding')});
			}else{
				file = new Blob([data], {type: xhr.getResponseHeader('content-type'), encoding: xhr.getResponseHeader('content-encoding')});
			}

			url = window.URL.createObjectURL(file);
			a.attr('href', url);
			a.attr('download', xhr.getResponseHeader('filename'));
			jQuery('body').append(a);
			a[0].click();
			window.URL.revokeObjectURL(url);
			a.remove();
		}
	});
}

/**
 * Makes an ajax post to the save_units function
 * @param  {obj} data  The form data
 * @param  {fn} _callback Callback function
 * @return {fn}           The callback
 */
function saveUnitInfo(data, _callback){
	data = {
		action: 'save_units',
		data: data
	};

	jQuery.ajax({
		type: 'POST',
		url: siteUrl + '/wp-admin/admin-ajax.php',
		data: data,
		success: function(data){
			location.reload();
		}
	});
}

/**
 * Makes an ajax post to the save_units function
 * @param  {obj} data  The form data
 * @param  {fn} _callback Callback function
 * @return {fn}           The callback
 */
function updateUnitEntry(data, _callback){
	data = {
		action: 'update_unit_entry',
		data: data
	};

	jQuery.ajax({
		type: 'POST',
		url: siteUrl + '/wp-admin/admin-ajax.php',
		data: data,
		success: function(data){
			if(typeof _callback === 'function') return _callback(data);
		}
	});
}

/**
 * Attaches google map to a DOM element
 * @param  {Object} elem The HTML element that you want to attach the map to
 * @param  {string} [name='My Location'] Name on the marker
 * @param  {Object} coords Object containing lat and lng strings
 * @return {Boolean} Returns false if required params are missing.
 */
function initMap(elem, name, coords){
	var map, marker;

	name = name || 'My Location';
	map = new google.maps.Map(elem, {
		center: coords,
		zoom: 18,
		scrollwheel: false
	});

	marker = new google.maps.Marker({
		position: coords,
		map: map,
		title: name
	});

	return true;
}

/**
 * Codes an addrress string to a lat/lng object and sends it to the map making function
 * @param  {Object} elem The HTML element that you want to attach the map to. Passed to initMap
 * @param  {string} [name='My Location'] Name on the marker. Passed to initMap
 * @param  {string} address Address string you want to encode
 * @return {Boolean|Object} Google geocoder object. Returns false if required params are missing.
 */
function codeAddress(elem, name, address, makeMap){
	var geocoder = new google.maps.Geocoder();

	name = name || 'My Location';
	return geocoder.geocode({
		'address': address
	}, function(results, status){
		if(status === google.maps.GeocoderStatus.OK && makeMap){
			initMap(elem, name, {
				lat: results[0].geometry.location.lat(),
				lng: results[0].geometry.location.lng()
			});
		}
	});
}

jQuery(document).ready(function($){
	(function(){
		var ni;
		var editors = [
			{name: 'acf-schema_content', type: 'json'},
			{name: 'acf-arbitrary_code_head_content', type: 'html'},
			{name: 'acf-arbitrary_code_begin_body_content', type: 'html'},
			{name: 'acf-arbitrary_code_end_body_content', type: 'html'}
		];

		for(ni = 0; ni < editors.length; ++ni){
			let editorId = '#' + editors[ni].name;

			if($(editorId).length){
				$(editorId).after('<div id="ace-editor_' + editors[ni].name + '" style="width: 100%; height: 300px"></div>');
				ace.require('ace/ext/language_tools');

				let Range = ace.require('ace/range').Range;
				let editor = ace.edit('ace-editor_' + editors[ni].name);

				editor.setTheme('ace/theme/nord_dark');
				editor.session.setMode('ace/mode/' + editors[ni].type);
				editor.session.setTabSize(2);
				editor.setHighlightActiveLine(true);
				editor.setShowPrintMargin(false);
				editor.setDisplayIndentGuides(true);
				editor.getSession().setUseWrapMode('soft');
				editor.getSession().setUseSoftTabs(true);
				editor.renderer.setScrollMargin(10, 10, 0, 0);
				editor.session.setOption('indentedSoftWrap', false);

				editor.setValue($(editorId).val());
				editor.selection.setRange(new Range(0, 0, 0, 0));

				editor.session.on('change', function(delta){
					$(editorId).val(editor.getValue());
				});
			}
		}
	})();

	/**
	 * Pulls up the in-post WP media manager and sets values for the media object when selected
	 * @param  {object} self The Scope of the element that was clicked
	 */
	function makeMediaGallery(self){
		if(!self) return;

		var metaImageFrame,
			mediaAttachment;

		if(metaImageFrame){
			metaImageFrame.open();
			return;
		}

		metaImageFrame = wp.media.frames.metaImageFrame = wp.media({
			// title: meta_image.title,
			// button: { text:  meta_image.button },
			// library: { type: 'image' }
		});

		metaImageFrame.on('select', function(){
			mediaAttachment = metaImageFrame.state().get('selection').first().toJSON();

			jQuery(self).nextAll('#logo').val(JSON.stringify(mediaAttachment.id));
			jQuery(self).attr('src', mediaAttachment.sizes.thumbnail.url);
		});

		metaImageFrame.open();
	}

	$('#customize-theme-image-preview').click(function(e){
		e.preventDefault();
		makeMediaGallery(this);
	});

	$('#upload-image-button').click(function(e){
		e.preventDefault();
		makeMediaGallery($('#customize-theme-image-preview')[0]);
	});

	$('.tab-section .hidden').removeClass('hidden');
	$('.tab-section').tabs();

	$('#enable_analytics').change(function(){
		if(this.checked){
			$('.type-analytics').removeAttr('readonly');
		}else{
			$('.type-analytics').attr('readonly', 'true');
		}
	});

	// TODO with gutenberg, the dom takes forever to load and even with this in window.load it misses the element.
	// need to find some other place to hook this is. also they changed the names of the selects so not even sure if the parent
	// select id will even be the same each time.
	$('#inspector-select-control-2').on('change', function(){
		var that = $(this),
			data = {
				action: 'get_page_templates',
				data: {
					post_id: postId,
					post_parent: that.val()
				}
			};

		$.ajax({
			type: 'GET',
			url: siteUrl + '/wp-admin/admin-ajax.php',
			data: data,
			success: function(data){
				var out = '<option value="default">Default Template</option>',
					name;

				data = JSON.parse(data);
				for(name in data){
					out += '<option value="' + data[name] + '">' + name + '</option>';
				}

				$('.editor-page-attributes__template select').html(out);
			}
		});
	});

	$('.import-google-cal').on('click', function(e){
		e.preventDefault();
		importGoogleCalendar();
	});

	$('.submissions-download-csv').on('click', function(e){
		e.preventDefault();
		downloadSubmissionCSV();
	});

	if($('#event-location-map').length && eventAddress){
		codeAddress($('#event-location-map')[0], 'My event location', eventAddress, true);
	}

	if(typeof repeating !== 'undefined'){
		$('#repeating-properties').show();

		if(recurrence.FREQ === 'DAILY'){
			$('#interval-wrapper').hide();
		}

		if(recurrence.FREQ === 'WEEKLY'){
			$('#recurr_days').show();
		}

		if(recurrence.FREQ === 'MONTHLY'){
			$('#recurr_by').show();
		}
	}

	if(typeof noLocation !== 'undefined' && !noLocation){
		$('#event_location').slideDown();
	}

	$('.location-input').on('input', debounce(function(){
		if($(this).val()){
			codeAddress($('#event-location-map')[0], 'My event location', $(this).val(), true);
		}
	}, 750));

	$('#all_day').on('change', function(){
		var that = $(this);

		$('.event-time').each(function(){
			$(this).attr('disabled', that.prop('checked'));
		});
	});

	$('#recurring').on('change', function(){
		var that = $(this);
		repeating = that.prop('checked');

		if(repeating){
			$('#repeating-properties').slideDown();
		}else{
			$('#repeating-properties').slideUp();
		}
	});

	$('#noLocation').on('change', function(){
		var that = $(this);
		noLocation = that.prop('checked');

		if(!noLocation){
			$('#event_location').slideDown();
		}else{
			$('#event_location').slideUp();
		}
	});

	$('#freq').on('change', function(){
		var that = $(this);

		if(that.val() === 'DAILY'){
			$('#interval-wrapper').hide();
		}else{
			$('#interval-wrapper').show();
		}

		if(that.val() === 'WEEKLY'){
			$('#recurr_days').show();
		}else{
			$('#recurr_days').hide();
		}

		if(that.val() === 'MONTHLY'){
			$('#recurr_by').show();
		}else{
			$('#recurr_by').hide();
		}
	});

	$('.add-exception').click(function(e){
		var item = $(this).parent(),
			input = item.find('input');

		e.preventDefault();
		if(input.val()){
			let copy = item.clone();

			copy.find('button')
				.addClass('remove-exception')
				.removeClass('add-exception')
				.text('-')
				.click(function(e){
					e.preventDefault();
					$(this).parent().remove();
				});
			item.after(copy);
			input.val('');
		}
	});

	$('.remove-exception').click(function(e){
		e.preventDefault();
		$(this).parent().remove();
	});

	/**
	 * Adds a new item to the form builder. Sets name attribtes to the newly created item elements
	 */
	$('#form-builder-input-types li[class*="add-field-item-"] a').on('click', function(e){
		var $this = $(this),
			item = document.createElement('div'),
			input,
			type,
			inputTypes = {
				'text': {
					label: 'Text',
					iconClass: 'dashicons-editor-textcolor',
					multi: false
				},
				'textarea': {
					label: 'Text Area',
					iconClass: 'dashicons-editor-alignleft',
					multi: false
				},
				'number': {
					label: 'Number',
					iconClass: 'dashicons-calculator',
					multi: false
				},
				'email': {
					label: 'Email',
					iconClass: 'dashicons-email',
					multi: false
				},
				'tel': {
					label: 'Phone',
					iconClass: 'dashicons-phone',
					multi: false
				},
				'date': {
					label: 'Date',
					iconClass: 'dashicons-calendar-alt',
					multi: false
				},
				'file': {
					label: 'File',
					iconClass: 'dashicons-media-default',
					multi: false
				},
				'select': {
					label: 'Select',
					iconClass: 'dashicons-editor-ul',
					multi: true
				},
				'radio': {
					label: 'Radio',
					iconClass: 'dashicons-marker',
					multi: true
				},
				'checkbox': {
					label: 'Checkbox',
					iconClass: 'dashicons-saved',
					multi: true
				}
			};

		e.preventDefault();
		type = $this.data('type');
		input = inputTypes[type];
		item = $(item);

		item.addClass('form-item width-100');
		item.html($('.empty-field.screen-reader-text').html());
		//		item.attr('name', 'item[]');
		item.find('h2').text(input.label + ' input').addClass(input.iconClass);
		item.find('.label-input').attr('name', 'label[]').attr('required', 'true');
		item.find('.required-input').attr('name', 'required[]');
		item.find('.type-input').attr('name', 'type[]').val(type);
		item.find('.width-input').attr('name', 'width[]');
		item.find('.remove-form-item').on('click', function(){
			$(this).parents('.form-item').remove();
			return false;
		});

		item.find('.width-select').on('click', function(e){
			e.preventDefault();
			$(this).parents('.form-item').removeClass('width-100 width-75 width-66 width-50 width-33 width-25');
			$(this).parents('.form-item').addClass('width-' + $(this).data('width'));
			item.find('.width-input').val($(this).data('width'));

			return false;
		});

		$('#form-builder-fields .row').append(item);
		item.find('input[class*="input"], textarea[class*="input"]').each(function(){
			var that = $(this);

			that.attr('id', that.attr('id').replace('new', item.index() + 1));

			if(typeof qTranslateConfig === 'object'){
				qTranslateConfig.qtx.addContentHooks(that);
			}
		});

		if(type === 'email'){
			item.find('.input-sizes').after('<div>validator</div>');
		}

		if(input.multi){
			item.find('.placeholder-input').parent().before(
				`<div class="form-input-wrapper">
					Fields Options
					<div class="multi-items">
						<div class="options-wrap">
							<input class="widefat" type="text" name="multi[input_${item.index()}][]">
							<div class="remove-option">&#10006;</div>
						</div>
					</div>
					<input id="placeholder-input-${item.index() + 1}" type="hidden" class="widefat placeholder-input i18n-multilingual" placeholder="Placeholder" value="">
					<a class="multi-add">Add</a>
				</div>`
			).remove();

			item.find('.multi-add').on('click', function(e){
				var opt = $(`<div class="options-wrap"><input class="widefat" type="text" name="multi[input_${item.index()}][]"><div class="remove-option">&#10006;</div></div>`);

				e.preventDefault();
				item.find('.multi-items').append(opt);
				item.find('.remove-option').on('click', function(){
					$(this).parent().remove();
				});

				opt.find('input').focus();
			});

			item.find('.remove-option').on('click', function(e){
				e.preventDefault();
				$(this).parent().remove();
			});
		}

		item.find('.placeholder-input').attr('name', 'placeholder[]');
		item.find('.required-toggle').on('change', function(){
			$(this).prev('input.required-input').val(this.checked);
		});

		return false;
	});

	$('.multi-add').on('click', function(e){
		var parent = $(this).closest('.form-item'),
			opt = $(`<div class="options-wrap"><input class="widefat" type="text" name="multi[input_${parent.index()}][]"><div class="remove-option">&#10006;</div></div>`);

		e.preventDefault();
		parent.find('.multi-items').append(opt);
		opt.find('input').focus();
	});

	$('.remove-option').on('click', function(e){
		e.preventDefault();
		$(this).parent().remove();
	});

	$('.remove-form-item').on('click', function(e){
		e.preventDefault();
		$(this).parents('.form-item').remove();
		return false;
	});

	$('.width-select').on('click', function(e){
		e.preventDefault();
		$(this).parents('.form-item').removeClass('width-100 width-75 width-66 width-50 width-33 width-25');
		$(this).parents('.form-item').addClass('width-' + $(this).data('width'));
		$(this).parents('.form-item').find('.width-input').val($(this).data('width'));

		return false;
	});

	$('#form-builder-fields .ui-sortable').sortable({
		handle: '.drag-handle',
		placeholder: 'drag-placeholder',
		forcePlaceholderSize: true,
		start: function(event, ui){
			ui.placeholder.height(ui.item.height());
		},
		stop(event, ui){
			$('.form-item').each(function(){
				var that = $(this);

				that.find('[id*="-input-"]').each(function(){
					var elem = $(this),
						id = elem.attr('id');

					elem.attr('id', id.replace(/[0-9]*$/, that.index() + 1));
				});

				that.find('[name^="multi"]').each(function(){
					var elem = $(this),
						name = elem.attr('name');

					elem.attr('name', name.replace(/[0-9]/, that.index()));
				});
			});
		}
	});

	$('.required-toggle').on('change', function(){
		$(this).prev('input.required-input').val(this.checked);
	});

	$('.save-unit-info').on('click', function(e){
		e.preventDefault();
		if(typeof qTranslateConfig === 'object'){
			var allUnitsInputs = $('input[name^="qtranslate-fields[all_unit_info]"]'),
				singleUnitsInputs = $('input[name^="qtranslate-fields[single_unit_info]"]'),
				allUnitsContent = '[:]',
				singleUnitsContent = '{:}',
				data;

			allUnitsInputs.each(function(index){
				if(index === allUnitsInputs.length - 1) return;
				allUnitsContent = $(this).attr('name').replace(/.*(?=\[([a-zA-z_]+?)]*$).*/, '[:$1]') + $(this).val() + allUnitsContent;
			});

			singleUnitsInputs.each(function(index){
				if(index === singleUnitsInputs.length - 1) return;
				singleUnitsContent = $(this).attr('name').replace(/.*(?=\[([a-zA-z_]+?)]*$).*/, '{:$1}') + $(this).val() + singleUnitsContent;
			});

			data = {
				unit_info_meta_box_nonce: $('#unit_info_meta_box_nonce').val(),
				all_unit_info: allUnitsContent,
				single_unit_info: singleUnitsContent
			};

			saveUnitInfo(data);
		}else{
			saveUnitInfo(urlParams($('#unit_info_meta_box_nonce, #all_unit_info, #single_unit_info').serialize()));
		}
	});

	$('#unit-list td span').click(function(e){
		e.preventDefault();
		var that = $(this),
			input = that.next('input');

		input.focus(function(){
			var val = input.val();

			$(window).on('keydown', function(e){
				if(e.which === 27){
					e.preventDefault();
					input.val(val).blur();
					$(window).off('keydown');
				}

				if(e.which === 13){
					e.preventDefault();
					input.blur();
					$(window).off('keydown');
				}
			});
		});

		input.blur(function(){
			input.hide();
			that.show();
		});

		that.hide().next('input').show().focus();

		input.change(function(){
			var rowId = input.closest('tr').data('id'),
				displayVal = input.val(),
				data = {
					column: input.attr('id').split('-')[0],
					unit_info_meta_box_nonce: $('#unit_info_meta_box_nonce').val(),
					row_id: rowId
				};

			if(input.attr('id').indexOf('affordable') >= 0){
				displayVal = input.prop('checked') ? 'Yes' : 'No';
				input.val(input.prop('checked') ? 1 : 0);
			}else if(input.attr('id').indexOf('SqFt') >= 0){
				displayVal += ' ft.<sup>2</sup>';
			}else if(input.attr('id').indexOf('price') >= 0){
				displayVal = '$' + displayVal.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
			}
			if(input.attr('type') === 'number'){
				data.value = parseInt(input.val(), 10);
			}else{
				data.value = input.val();
			}

			that.html(displayVal);
			updateUnitEntry(data);
		});
	});

	$('#unit-list td.available input[type="checkbox"]').change(function(){
		var that = $(this),
			rowId = that.closest('tr').data('id'),
			data = {
				column: that.attr('id').split('-')[0],
				unit_info_meta_box_nonce: $('#unit_info_meta_box_nonce').val(),
				row_id: rowId,
				value: that.prop('checked') ? 1 : 0
			};

		updateUnitEntry(data);
	});

	/**
	 * Makes an ajax post to save the restaurants option page
	 * @param  {obj} data  The form data
	 * @param  {fn} _callback Callback function
	 * @return {fn}           The callback
	 */
	$('#customize-theme-form').submit(function(e){
		var data = {
			action: 'customize_theme_save',
			data: urlParams($(this).serialize())
		};

		e.preventDefault();

		jQuery.ajax({
			type: 'POST',
			url: siteUrl + '/wp-admin/admin-ajax.php',
			data: data,
			success: function(data){
				location.reload();
			}
		});
	});
});

/**
 * this updates the qtrans fields on blur, but they dont exist until well after doc.ready
 */
jQuery(window).on('load', function(){
	if(typeof qTranslateConfig === 'object'){
		$('.qtranxs-translatable').change(function(){
			var that = jQuery(this);

			that.prevAll('input[name="qtranslate-fields[' + that.attr('name') + '][' + qTranslateConfig.activeLanguage + ']"]').val(that.val());
		});
	}
});
