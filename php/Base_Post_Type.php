<?php
/**
 * This is a template file for creating a base post type. Replace all instances of "Base" and
 * "Bases" with your desired post labels, then include this in the functions.php file
 */
Base::init();

Class Base{

	private static $options_post;

	public function __construct(){}

	public static function init(){
		add_action('init', array(__CLASS__, 'create_post_type'));
		add_action('init', array(__CLASS__, 'create_taxonomy'));
		add_action('admin_menu', array(__CLASS__, 'add_archive_options_menu'));

		self::$options_post = get_option('base_archive_options');

		if(!isset(self::$options_post) || empty(self::$options_post)){
			self::$options_post = wp_insert_post(array('post_title' => 'Base Archive Options', 'post_type' => 'post-archive-options'));

			update_option('base_archive_options', self::$options_post);
		}
	}

	/**
	 * Create the post type for FAQs.
	 */
	function create_post_type(){
		register_post_type('base',
			array(
				'labels' => array(
					'name'          => __('Bases', 'starter_basic_admin'),
					'singular_name' => __('Base', 'starter_basic_admin'),
					'archives'      => __('Bases', 'starter_basic'),
					'menu_name'     => __('Bases', 'starter_basic_admin'),
				),
				'hierarchical'       => true,
				'supports'           => array('title', 'editor', 'page-attributes'),
				'menu_icon'          => '',
				'public'             => true,
				'has_archive'        => true,
				'publicly_queriable' => true,
				'rewrite'            => array('slug' => 'bases'),
			)
		);
	}

	/**
	 * Create category taxonomy
	 */
	function create_taxonomy(){
		register_taxonomy(
			'base-category',
			'base',
			array(
				'label'             => __('Base Categories', 'starter_basic_admin'),
				'rewrite'           => true,
				'hierarchical'      => true,
				'show_in_nav_menus' => false,
			)
		);
	}

	/**
	 * Undocumented function
	 */
	function add_archive_options_menu(){
		add_submenu_page('edit.php?post_type=base', __('Base Archive Options', 'starter_basic_admin'), __('Base Archive Options', 'starter_basic_admin'), 'activate_plugins', 'post.php?post=' . self::$options_post . '&action=edit');
	}
}
