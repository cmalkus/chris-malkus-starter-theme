<?php

Auth::init();

Class Auth{
	private static $captcha_site_key;
	private static $captcha_secret_key;
	private static $captcha_have_api_keys = 0;

	public function __construct(){}

	public static function init(){
		self::$captcha_site_key = get_theme_mod('recaptcha_site_key');
		self::$captcha_secret_key = get_theme_mod('recaptcha_secret_key');
		self::$captcha_have_api_keys = isset(self::$captcha_site_key) && !empty(self::$captcha_site_key) && isset(self::$captcha_secret_key) && !empty(self::$captcha_secret_key);

		add_action('wp_ajax_nopriv_user_login', array(__CLASS__, 'user_login'));
		add_action('wp_ajax_user_logout', array(__CLASS__, 'user_logout'));
		add_action('wp_ajax_nopriv_user_signup', array(__CLASS__, 'user_signup'));
	}

	/**
	 * Logs the user in.
	 */
	function user_login(){
		$response = new stdClass();
		$response->errors = [];
		$response->status = 0;

		if(!empty($_POST['data']['user_email']) && !empty($_POST['data']['user_password'])){
			$user_login = esc_attr($_POST['data']['user_email']);
			$user_password = esc_attr($_POST['data']['user_password']);
			$user_remember = isset($_POST['data']['user_remember']);

			unset($_POST['data']);

			$creds = array();
			$creds['user_login'] = $user_login;
			$creds['user_password'] = $user_password;
			$creds['remember'] = $user_remember;

			if(!is_user_logged_in()){
				$user = wp_signon($creds, false);
				wp_set_current_user($user->ID);

				if(is_wp_error($user)){
					$response->errors[] = strip_tags($user->get_error_message());
				}
			}
		} else {
			$response->errors[] = __('Both username and password are required.', 'starter_basic');
		}

		if(empty($response->errors)){
			$response->status = 1;
			$response->name = $user->display_name;
			$response->cleanName = $user->user_nicename;
		} else {
			$response->status = -count($response->errors);
		}
		header('Content-Type:application/json;');
		echo json_encode($response);
		wp_die();
	}

	/**
	 * Logs the user out.
	 */
	function user_logout(){
		$response = new stdClass();
		$response->errors = [];
		$response->status = 0;

		if(!empty($_POST['data']['user_logout']) && ($_POST['data']['user_logout'] == '1')){
			unset($_POST['data']);

			if(is_user_logged_in()){
				wp_logout();
				wp_set_current_user(0);
			}
		} else {
			$response->errors[] = __('The form is empty somehow.', 'starter_basic');
		}

		if(empty($response->errors)){
			$response->status = 1;
		} else {
			$response->status = -count($response->errors);
		}
		header('Content-Type:application/json;');
		echo json_encode($response);
		wp_die();
	}

	/**
	 * Signs the user up. New user has no role and can't access anything. They do get their own page though.
	 */
	function user_signup(){
		$response = new stdClass();
		$response->errors = [];
		$response->status = 0;

		if(!empty($_POST['data']['user_email']) && !empty($_POST['data']['user_password']) && !empty($_POST['data']['user_verify_password'])){
			$username = sanitize_email($_POST['data']['user_email']);
			$password = esc_attr($_POST['data']['user_password']);
			$verify_password = esc_attr($_POST['data']['user_verify_password']);
			$email = sanitize_email($_POST['data']['user_email']);

			if(self::$captcha_have_api_keys && $_POST['g-recaptcha-response']){
				$captcha = $_POST['data']['g-recaptcha-response'];
				$user_ip = $_SERVER['REMOTE_ADDR'];

				$captcha_response = file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret=' . self::$captcha_secret_key . '&response=' . $captcha . '&remoteip=' . $user_ip);
				$response_keys = json_decode($captcha_response, true);
			}

			if(intval($response_keys['success']) === 1 || !self::$captcha_have_api_keys){
				if(filter_var($email, FILTER_VALIDATE_EMAIL)){
					if($verify_password === $password){
						$signup = wp_create_user($username, $password, $email);

						if(is_wp_error($signup)){
							$response->errors[] = strip_tags($signup->get_error_message());
						} else {
							$change_role = new WP_User($signup);
							$change_role->set_role('');

							static::user_login();
						}
					} else {
						$response->errors[] = __('Your passwords do not match.', 'starter_basic');
					}
				} else {
					$response->errors[] = __('Invalid email address.', 'starter_basic');
				}
			} else {
				$response->errors[] = __('Captcha Failed.', 'starter_basic');
			}

		} else {
			$response->errors[] = __('Missing information. Please try again', 'starter_basic');
		}

		unset($_POST['data']);

		if(empty($response->errors)){
			$response->status = 1;
			$response->name = $user->display_name;
		} else {
			$response->status = -count($response->errors);
		}
		header('Content-Type:application/json;');
		echo json_encode($response);
		wp_die();
	}
}
