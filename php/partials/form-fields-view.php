<div class="clearfix">
	<div class="form-builder-sidebar">
		<div id="form-builder-input-types">
			<h2><?php _e('Input Types', 'starter_basic_admin') ?></h2>
			<p><?php _e('Click an input type to add it to the form', 'starter_basic_admin') ?></p>
			<ul>
				<?php foreach(self::INPUT_TYPES as $key => $val): ?>
					<li class="add-field-item-<?php echo $key; ?>"><a href="" data-type="<?php echo $key; ?>"><?php echo $val['label']; ?></a></li>
				<?php endforeach; ?>
			</ul>
		</div>
		<div id="form-builder-behavior">
			<h2><?php _e('Behavior', 'starter_basic_admin') ?></h2>
			<p><?php _e('When this form is submitted, it should', 'starter_basic_admin') ?></p>
			<label>
				<input class="make-post-toggle" type="checkbox" name="make_post" <?php echo get_post_meta($post->ID, 'make_post', true) != null ? 'checked' : ''; ?>>
				<?php _e('Make a post', 'starter_basic_admin');?>
			</label>
			<label>
				<input class="send-email-toggle" type="checkbox" name="send_email" <?php echo get_post_meta($post->ID, 'send_email', true) != null ? 'checked' : ''; ?>>
				<?php _e('Send an email to', 'starter_basic_admin');?>
				<input type="email" name="to_emails" multiple value="<?php echo get_post_meta($post->ID, 'to_emails', true) ?>">
			</label>
			<hr>
			<p><?php echo sprintf(__('reCaptcha is enabled for all forms automatically if the propper keys have been entered %1$shere%2$s', 'starter_basic_admin'), '<a href="' . site_url() . '/wp-admin/options-general.php?page=customize_theme#site-integrations">', '</a>') ?></p>
		</div>
</div>
	<div id="form-builder-fields">
		<div class="row ui-sortable">
			<?php if($form_fields):
				foreach($form_fields as $i => $field){ ?>
					<div class="form-item width-<?php echo $field['width']; ?>">
						<div class="remove-form-item">&#10006;</div>
						<h2 class="dashicons-before <?php echo self::INPUT_TYPES[$field['type']]['icon_class']; ?> drag-handle"><?php echo sprintf(__('%s Input', 'starter_basic_admin'), self::INPUT_TYPES[$field['type']]['label']); ?></h2>
						<div>
							<label class="form-input-wrapper">
								<?php _e('Field Label', 'starter_basic_admin');?>
								<input id="label-input-<?php echo $i + 1; ?>" type="text" class="widefat label-input i18n-multilingual" name="label[]" placeholder="<?php _e('Label', 'starter_basic_admin');?>" value="<?php echo $field['label']; ?>" required>
							</label>
							<?php if(self::INPUT_TYPES[$field['type']]['multi']): ?>
								<div class="form-input-wrapper">
									<?php _e('Fields Options', 'starter_basic_admin') ?>
									<div class="multi-items">
										<?php foreach($field['options'] as $option): ?>
											<div class="options-wrap">
												<input class="widefat" type="text" name="multi[input_<?php echo $i; ?>][]" value="<?php echo $option; ?>">
												<div class="remove-option">&#10006;</div>
											</div>
										<?php endforeach; ?>
									</div>
									<input id="placeholder-input-<?php echo $i + 1; ?>" type="hidden" class="widefat placeholder-input i18n-multilingual" name="placeholder[]" placeholder="<?php _e('Placeholder', 'starter_basic_admin');?>" value="<?php echo $field['placeholder']; ?>">
									<a class="multi-add"><?php _e('Add', 'starter_basic_admin') ?></a>
								</div>
							<?php else: ?>
							<label class="form-input-wrapper">
								<?php _e('Field Placeholder', 'starter_basic_admin');?>
								<input id="placeholder-input-<?php echo $i + 1; ?>" type="text" class="widefat placeholder-input i18n-multilingual" name="placeholder[]" placeholder="<?php _e('Placeholder', 'starter_basic_admin');?>" value="<?php echo $field['placeholder']; ?>">
							</label>
							<?php endif; ?>
						</div>
						<div>
							<label class="form-input-wrapper">
								<input id="required-input-<?php echo $i + 1; ?>" class="required-input" type="hidden" value="<?php echo $field['required']; ?>" name="required[]">
								<input class="required-toggle" type="checkbox" <?php echo filter_var($field['required'], FILTER_VALIDATE_BOOLEAN) ? 'checked' : ''; ?>>
								<?php _e('Required', 'starter_basic');?>
							</label>
							<div class="form-input-wrapper input-sizes">
								<div><?php _e('Width', 'starter_basic_admin') ?></div>
								<a class="width-select" href="" data-width="100"><?php _e('Full', 'starter_basic_admin');?></a>
								<a class="width-select" href="" data-width="75"><?php _e('3/4', 'starter_basic_admin');?></a>
								<a class="width-select" href="" data-width="66"><?php _e('2/3', 'starter_basic_admin');?></a>
								<a class="width-select" href="" data-width="50"><?php _e('1/2', 'starter_basic_admin');?></a>
								<a class="width-select" href="" data-width="33"><?php _e('1/3', 'starter_basic_admin');?></a>
								<a class="width-select" href="" data-width="25"><?php _e('1/4', 'starter_basic_admin');?></a>
							</div>
						</div>
						<input id="type-input-<?php echo $i + 1; ?>" class="type-input" type="hidden" name="type[]" value="<?php echo $field['type']; ?>">
						<input id="width-input-<?php echo $i + 1; ?>" class="width-input" type="hidden" name="width[]" value="<?php echo $field['width']; ?>">
					</div>
				<?php

				} else :
			?>

			<?php endif;?>
		</div>
	</div>
</div>
<div class="empty-field screen-reader-text form-item width-100">
	<div class="remove-form-item">&#10006;</div>
	<h2 class="dashicons-before drag-handle"><?php _e('Text Input', 'starter_basic_admin');?></h2>
	<div>
		<label class="form-input-wrapper">
			<?php _e('Field Label', 'starter_basic_admin');?>
			<input id="label-input-new" type="text" class="widefat label-input i18n-multilingual" placeholder="<?php _e('Label', 'starter_basic_admin');?>">
		</label>
		<label class="form-input-wrapper">
			<?php _e('Field Placeholder', 'starter_basic_admin');?>
			<input id="placeholder-input-new" type="text" class="widefat placeholder-input i18n-multilingual" placeholder="<?php _e('Placeholder', 'starter_basic_admin');?>">
		</label>
	</div>
	<div>
		<label class="form-input-wrapper">
			<input id="required-input-new" class="required-input" type="hidden" value="false">
			<input class="required-toggle" type="checkbox">
			<?php _e('Required', 'starter_basic_admin');?>
		</label>
		<div class="form-input-wrapper input-sizes">
			<div><?php _e('Width', 'starter_basic_admin') ?></div>
			<a class="width-select" href="" data-width="100"><?php _e('Full', 'starter_basic_admin');?></a>
			<a class="width-select" href="" data-width="75"><?php _e('3/4', 'starter_basic_admin');?></a>
			<a class="width-select" href="" data-width="66"><?php _e('2/3', 'starter_basic_admin');?></a>
			<a class="width-select" href="" data-width="50"><?php _e('1/2', 'starter_basic_admin');?></a>
			<a class="width-select" href="" data-width="33"><?php _e('1/3', 'starter_basic_admin');?></a>
			<a class="width-select" href="" data-width="25"><?php _e('1/4', 'starter_basic_admin');?></a>
		</div>
	</div>
	<input id="type-input-new" class="type-input" type="hidden" value="text">
	<input id="width-input-new" class="width-input" type="hidden" value="100">
</div>
