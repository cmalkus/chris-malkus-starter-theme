<?php

$responsive_styles = array(
	'mobile'  => '',
	'tablet'  => '',
	'desktop' => '',
);

Block_Utils::init();

Class Block_Utils{
	const RESPONSIVE_VIEWS = array(
		'mobile' => 0,
		'tablet' => 481,
		'desktop' => 769
	);

	const ALLOWED_BLOCKS = array(
		"core/block",
		"core/image",
		"core/post-author",
		"core/post-author-biography",
		"core/post-comments-form",
		"core/post-content",
		"core/post-date",
		"core/post-excerpt",
		"core/post-featured-image",
		"core/post-terms",
		"core/post-title",
		"core/read-more",
		"core/shortcode",
		"core/button",
		"core/code",
		"core/heading",
		"core/html",
		"core/list",
		"core/list-item",
		"core/more",
		"core/paragraph",
    	"core/preformatted",
    	"core/pullquote",
    	"core/quote",
    	"core/separator",
		"core/spacer",
    	"core/table",
		"core/video",
	);

	function __construct(){}

	static function init(){
		add_action('init', array(__CLASS__, 'register_blocks'), 2);
		add_action('wp_footer', array(__CLASS__, 'write_styles'));
		add_action('enqueue_block_editor_assets', array(__CLASS__, 'write_styles'));
		add_filter('acf/blocks/wrap_frontend_innerblocks', '__return_false', 10, 2); // remove "acf-innerblocks-container" wrapper div from innerBlocks
		add_action('after_setup_theme', array(__CLASS__, 'pattern_registry'));
		add_filter('allowed_block_types', array(__CLASS__, 'filter_block_types'), 10, 2);
		add_filter('render_block', array(__CLASS__, 'render_block'), 10, 2);
	}

	static function register_blocks(){
		register_block_type(get_template_directory() . '/php/blocks/section');
		register_block_type(get_template_directory() . '/php/blocks/container');
	}

	static function pattern_registry() {
		remove_theme_support('core-block-patterns');

		// toto make block patterns
		if (class_exists('WP_Block_Patterns_Registry')) {
			foreach(require_once 'block_patterns.php' as $name => $pattern){
				register_block_pattern($name, $pattern);
			}

			// register categories
			register_block_pattern_category('section', array(
				'label' => __('Section', 'starter_basic'),
			));
		}
	}

	static function filter_block_types($allowed_blocks, $editor_context){
		$all_blocks = array_keys(WP_Block_Type_Registry::get_instance()->get_all_registered());
		$custom_blocks = array_filter($all_blocks, function($item){ return !str_starts_with($item, 'core/'); });
		$merged = array_merge(self::ALLOWED_BLOCKS, $custom_blocks);

		return $merged;

	}

	static function render_block($block_content, $block){
		self::geterate_style_sheet($block);

		return $block_content;
	}

	public static function geterate_style_sheet($block){
		if(!isset($block['attrs']['responsiveStyle'])) return;
		global $responsive_styles;

		$block_id = $block['attrs']['blockId'];

		foreach(array_reverse($block['attrs']['responsiveStyle']) as $view => $view_styles){
			$styles = '';
			// error_log(print_r($view_styles, 1));
			foreach($view_styles as $style => $value){
				switch($style){
					case 'spacing':
						foreach($value as $prop => $val){
							foreach($val as $pos => $val){
								$styles .= "$prop-$pos:$val;";
							}
						}
						break;
					case 'image':
						break;
					case 'color':
						foreach($value as $prop => $val){
							switch($prop){
								case 'background':
									$styles .= "background-color:$val;";
									break;
								case 'text':
									$styles .= "color:$val;";
									break;
								case 'gradient':
									$styles .= "background-image:$val;";
									break;
								default:
									break;
							}
						}
						break;
					case 'typography':
						foreach($value as $prop => $val){
							$prop = strtolower(implode('-', preg_split('/(?=[A-Z])/', $prop)));
							$styles .= "$prop:$val;";
						}
						break;
					default:
						break;
				}
			}

			$responsive_styles[$view] .= "[data-block-id=\"$block_id\"]{{$styles}}";
		}

		return;
	}

	static function write_styles(){
		global $responsive_styles;
		$stylesheet_content = '';

		foreach($responsive_styles as $view => $styles){
			$width = self::RESPONSIVE_VIEWS[$view];

			if ($view === 'mobile') {
				$stylesheet_content .= "{$styles}";
			} else {
				$stylesheet_content .= "@media(min-width: ${width}px){{$styles}}";
			}
		}

		echo "<style id=\"page-builder-styles\">$stylesheet_content</style>";
		return;
	}
}
