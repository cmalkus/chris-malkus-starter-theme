<?php

Customize_Theme::init();

Class Customize_Theme{
	private static $logo;
	private static $phone_number;
	private static $email;
	private static $street_address_1;
	private static $street_address_2;
	private static $city;
	private static $state;
	private static $postal;

	private static $twitter_link;
	private static $facebook_link;
	private static $instagram_link;
	private static $youtube_link;

	private static $enable_analytics;
	private static $google_analytics_code;
	private static $google_tags_code;
	private static $google_maps_code;
	private static $recaptcha_site_key;
	private static $recaptcha_secret_key;
	private static $fb_pixel_code;

	private static $arbitrary_code_head;
	private static $arbitrary_code_begin_body;
	private static $arbitrary_code_end_body;

	private static $enable_alert_bar;
	private static $alert_bar_content;

	private static $about_page_mapping;
	private static $contact_page_mapping;
	private static $site_map_page_mapping;

	function __construct(){}

	static function init(){
		self::$logo = get_theme_mod('logo');
		self::$phone_number = get_theme_mod('phone_number');
		self::$email = get_theme_mod('email');
		self::$street_address_1 = get_theme_mod('street_address_1');
		self::$street_address_2 = get_theme_mod('street_address_2');
		self::$city = get_theme_mod('city');
		self::$state = get_theme_mod('state');
		self::$postal = get_theme_mod('postal');

		self::$twitter_link = get_theme_mod('twitter_link');
		self::$facebook_link = get_theme_mod('facebook_link');
		self::$instagram_link = get_theme_mod('instagram_link');
		self::$youtube_link = get_theme_mod('youtube_link');

		self::$enable_analytics = get_theme_mod('enable_analytics');
		self::$google_analytics_code = get_theme_mod('google_analytics_code');
		self::$google_tags_code = get_theme_mod('google_tags_code');
		self::$google_maps_code = get_theme_mod('google_maps_code');
		self::$recaptcha_site_key = get_theme_mod('recaptcha_site_key');
		self::$recaptcha_secret_key = get_theme_mod('recaptcha_secret_key');
		self::$fb_pixel_code = get_theme_mod('fb_pixel_code');

		self::$arbitrary_code_head = get_theme_mod('arbitrary_code_head');
		self::$arbitrary_code_begin_body = get_theme_mod('arbitrary_code_begin_body');
		self::$arbitrary_code_end_body = get_theme_mod('arbitrary_code_end_body');

		self::$enable_alert_bar = get_theme_mod('enable_alert_bar');
		self::$alert_bar_content = get_theme_mod('alert_bar_content');

		self::$about_page_mapping = get_option('about-page-mapping', true);
		self::$contact_page_mapping = get_option('contact-page-mapping', true);
		self::$site_map_page_mapping = get_option('site-map-page-mapping', true);

		add_action('admin_menu', array(__CLASS__, 'add_customize_menu'));
		add_action('customize_register', array(__CLASS__, 'customize_theme_register_settings'));
		add_action('add_meta_boxes', array(__CLASS__, 'customize_theme_register_meta_boxes'), 1);
		add_action('wp_ajax_customize_theme_save', array(__CLASS__, 'customize_theme_meta_box_save'));

	}

	/**
	 * Adds the theme customize seetings menu item
	 * @return void
	 */
	static function add_customize_menu(){
		add_submenu_page(
			'options-general.php',
			__('Site', 'starter_basic_admin'),
			__('Site', 'starter_basic_admin'),
			'activate_plugins',
			'customize_theme',
			array(__CLASS__, 'render_customize_page')
		);
	}

	/**
	 * Undocumented function
	 *
	 * @param WP_Customize $wp_customize
	 * @return void
	 */
	function customize_theme_register_settings($wp_customize){
		$wp_customize->add_setting('logo', array(
			'default' => '',
			'transport' => 'refresh',
		));

		$wp_customize->add_setting('phone_number', array(
			'default' => '',
			'transport' => 'refresh',
		));

		$wp_customize->add_setting('email', array(
			'default' => '',
			'transport' => 'refresh',
		));

		$wp_customize->add_setting('street_address_1', array(
			'default' => '',
			'transport' => 'refresh',
		));

		$wp_customize->add_setting('street_address_2', array(
			'default' => '',
			'transport' => 'refresh',
		));

		$wp_customize->add_setting('city', array(
			'default' => '',
			'transport' => 'refresh',
		));

		$wp_customize->add_setting('state', array(
			'default' => '',
			'transport' => 'refresh',
		));

		$wp_customize->add_setting('postal', array(
			'default' => '',
			'transport' => 'refresh',
		));

		$wp_customize->add_setting('twitter_link', array(
			'transport' => 'refresh',
		));

		$wp_customize->add_setting('facebook_link', array(
			'transport' => 'refresh',
		));

		$wp_customize->add_setting('instagram_link', array(
			'transport' => 'refresh',
		));

		$wp_customize->add_setting('youtube_link', array(
			'transport' => 'refresh',
		));

		$wp_customize->add_setting('google_analytics_code', array(
			'default' => '',
			'transport' => 'refresh',
		));

		$wp_customize->add_setting('google_tags_code', array(
			'default' => '',
			'transport' => 'refresh',
		));

		$wp_customize->add_setting('google_maps_code', array(
			'default' => '',
			'transport' => 'refresh',
		));

		$wp_customize->add_setting('fb_pixel_code', array(
			'default' => '',
			'transport' => 'refresh',
		));

		$wp_customize->add_setting('recaptcha_site_key', array(
			'default' => '',
			'transport' => 'refresh',
		));

		$wp_customize->add_setting('recaptcha_secret_key', array(
			'default' => '',
			'transport' => 'refresh',
		));

		$wp_customize->add_setting('arbitrary_code_head', array(
			'default' => '',
			'transport' => 'refresh',
		));

		$wp_customize->add_setting('arbitrary_code_begin_body', array(
			'default' => '',
			'transport' => 'refresh',
		));

		$wp_customize->add_setting('arbitrary_code_end_body', array(
			'default' => '',
			'transport' => 'refresh',
		));
	}

	static function customize_theme_register_meta_boxes(){
		add_meta_box(
			'site-information',
			__('Site Information', 'starter_basic_admin'),
			array(__CLASS__, 'customize_theme_site_info_fields'),
			'settings_customize_theme',
			'normal',
			'default'
		);

		add_meta_box(
			'arbitrary-code',
			__('Custom Code', 'starter_basic_admin'),
			array(__CLASS__, 'customize_theme_arbitrary_code_fields'),
			'settings_customize_theme',
			'normal',
			'default'
		);

		add_meta_box(
			'alert-bar',
			__('Alert Bar', 'starter_basic_admin'),
			array(__CLASS__, 'customize_theme_alert_bar'),
			'settings_customize_theme',
			'normal',
			'default'
		);

		add_meta_box(
			'settings-actions',
			__('Publish', 'starter_basic_admin'),
			array(__CLASS__, 'customize_theme_actions'),
			'settings_customize_theme',
			'side',
			'default'
		);

		add_meta_box(
			'company-logo',
			__('Logo', 'starter_basic_admin'),
			array(__CLASS__, 'customize_company_logo'),
			'settings_customize_theme',
			'side',
			'default'
		);
	}

	static function customize_theme_site_info_fields(){ ?>
		<div id="info-tabs" class="tab-section">
			<ul class="category-tabs clearfix">
				<li><a href="#company-info"><?php _e('Site Information', 'starter_basic_admin'); ?></a></li>
				<li><a href="#social-media"><?php _e('Social Media', 'starter_basic_admin'); ?></a></li>
				<li><a href="#integrations"><?php _e('Integrations', 'starter_basic_admin'); ?></a></li>
				<li><a href="#page-mapping"><?php _e('Page Mapping', 'starter_basic_admin'); ?></a></li>
			</ul>
			<br class="clear" />
			<div id="company-info">
				<table class="form-table" role="presentation">
					<tbody>
						<tr>
							<th scope="row"><label for="phone_number"><?php _e('Phone Number', 'starter_basic_admin') ?></label></th>
							<td><input id="phone_number" name="phone_number" class="regular-text" type="tel" value="<?php echo self::$phone_number ?>"></td>
						</tr>
						<tr>
							<th scope="row"><label for="email"><?php _e('Email', 'starter_basic_admin') ?></label></th>
							<td><input id="email" name="email" class="regular-text" type="email" value="<?php echo self::$email ?>"></td>
						</tr>
						<tr>
							<th scope="row"><label for="street_address_1"><?php _e('Street Address 1', 'starter_basic_admin') ?></label></th>
							<td><input id="street_address_1" name="street_address_1" class="regular-text" type="text" value="<?php echo self::$street_address_1 ?>"></td>
						</tr>
						<tr>
							<th scope="row"><label for="street_address_2"><?php _e('Street Address 1', 'starter_basic_admin') ?></label></th>
							<td><input id="street_address_2" name="street_address_2" class="regular-text" type="text" value="<?php echo self::$street_address_2 ?>"></td>
						</tr>
						<tr>
							<th scope="row"><label for="city"><?php _e('City', 'starter_basic_admin') ?></label></th>
							<td><input id="city" name="city" class="regular-text" type="text" value="<?php echo self::$city ?>"></td>
						</tr>
						<tr>
							<th scope="row"><label for="state"><?php _e('State', 'starter_basic_admin') ?></label></th>
							<td><input id="state" name="state" class="regular-text" type="text" value="<?php echo self::$state ?>"></td>
						</tr>
						<tr>
							<th scope="row"><label for="postal"><?php _e('Postal', 'starter_basic_admin') ?></label></th>
							<td><input id="postal" name="postal" class="regular-text" type="text" value="<?php echo self::$postal ?>"></td>
						</tr>
					</tbody>
				</table>
			</div>
			<div class="hidden" id="social-media">
				<table class="form-table" role="presentation">
					<tbody>
						<tr>
							<th scope="row"><label for="twitter_link"><?php _e('Twitter', 'starter_basic_admin') ?></label></th>
							<td><input id="twitter_link" name="twitter_link" class="regular-text" type="text" value="<?php echo self::$twitter_link ?>"></td>
						</tr>
						<tr>
							<th scope="row"><label for="facebook_link"><?php _e('Facebook', 'starter_basic_admin') ?></label></th>
							<td><input id="facebook_link" name="facebook_link" class="regular-text" type="text" value="<?php echo self::$facebook_link ?>"></td>
						</tr>
						<tr>
							<th scope="row"><label for="instagram_link"><?php _e('Instagram', 'starter_basic_admin') ?></label></th>
							<td><input id="instagram_link" name="instagram_link" class="regular-text" type="text" value="<?php echo self::$instagram_link ?>"></td>
						</tr>
						<tr>
							<th scope="row"><label for="youtube_link"><?php _e('YouTube', 'starter_basic_admin') ?></label></th>
							<td><input id="youtube_link" name="youtube_link" class="regular-text" type="text" value="<?php echo self::$youtube_link ?>"></td>
						</tr>
					</tbody>
				</table>
			</div>
			<div class="hidden" id="integrations">
				<table class="form-table" role="presentation">
					<tbody>
						<tr>
							<th scope="row"><label for="enable_analytics"><?php _e('Enable Analytics', 'starter_basic_admin') ?></label></th>
							<td><input id="enable_analytics" name="enable_analytics" type="checkbox" value="1" <?php echo self::$enable_analytics ? 'checked' : '' ?> ></td>
						</tr>
						<tr>
							<th scope="row"><label for="google_analytics_code"><?php _e('Google Analytics', 'starter_basic_admin') ?></label></th>
							<td><input id="google_analytics_code" class="type-analytics regular-text" name="google_analytics_code" type="text" value="<?php echo self::$google_analytics_code ?>" <?php echo !self::$enable_analytics ? 'readonly="true"' : '' ?>></td>
						</tr>
						<tr>
							<th scope="row"><label for="google_tags_code"><?php _e('Google tags', 'starter_basic_admin') ?></label></th>
							<td><input id="google_tags_code" class="type-analytics regular-text" name="google_tags_code" type="text" value="<?php echo self::$google_tags_code ?>"<?php echo !self::$enable_analytics ? 'readonly="true"' : '' ?>></td>
						</tr>

						<tr>
							<th scope="row"><label for="fb_pixel_code"><?php _e('Facebook Pixel', 'starter_basic_admin') ?></label></th>
							<td><input id="fb_pixel_code" class="type-analytics regular-text" name="fb_pixel_code" type="text" value="<?php echo self::$fb_pixel_code ?>"<?php echo !self::$enable_analytics ? 'readonly="true"' : '' ?>></td>
						</tr>
						<tr>
							<th scope="row"><label for="google_maps_code"><?php _e('Google Maps', 'starter_basic_admin') ?></label></th>
							<td><input id="google_maps_code" name="google_maps_code" class="regular-text" type="text" value="<?php echo self::$google_maps_code ?>"></td>
						</tr>
						<tr>
							<th scope="row"><label for="recaptcha_site_key"><?php _e('reCaptcha Site key', 'starter_basic_admin') ?></label></th>
							<td><input id="recaptcha_site_key" name="recaptcha_site_key" class="regular-text" type="text" value="<?php echo self::$recaptcha_site_key ?>"></td>
						</tr>
						<tr>
							<th scope="row"><label for="recaptcha_secret_key"><?php _e('reCaptcha Secret key', 'starter_basic_admin') ?></label></th>
							<td><input id="recaptcha_secret_key" name="recaptcha_secret_key" class="regular-text" type="text" value="<?php echo self::$recaptcha_secret_key ?>"></td>
						</tr>
					</tbody>
				</table>
			</div>
			<div class="hidden" id="page-mapping">
				<table class="form-table" role="presentation">
					<tbody>
						<tr>
							<th scope="row"><label for="about_page_mapping"><?php _e('About Page', 'starter_basic_admin') ?></label></th>
							<td><?php wp_dropdown_pages(array(
								'name' => 'about-page-mapping',
								'id' => 'about_page_mapping',
								'selected' => self::$about_page_mapping,
								'option_none_value' => '0',
								'show_option_none' => '- ' . __('Select', 'starter_basic_admin') . ' -',
							)); ?></td>
						</tr>
						<tr>
							<th scope="row"><label for="contact_page_mapping"><?php _e('Contact Page', 'starter_basic_admin') ?></label></th>
							<td><?php wp_dropdown_pages(array(
								'name' => 'contact-page-mapping',
								'id' => 'contact_page_mapping',
								'selected' => self::$contact_page_mapping,
								'option_none_value' => '0',
								'show_option_none' => '- ' . __('Select', 'starter_basic_admin') . ' -',
							)); ?></td>
						</tr>
						<tr>
							<th scope="row"><label for="site_map_page_mapping"><?php _e('Site Map Page', 'starter_basic_admin') ?></label></th>
							<td><?php wp_dropdown_pages(array(
								'name' => 'site-map-page-mapping',
								'id' => 'site_map_page_mapping',
								'selected' => self::$site_map_page_mapping,
								'option_none_value' => '0',
								'show_option_none' => '- ' . __('Select', 'starter_basic_admin') . ' -',
							)); ?></td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
	<?php
	}

	static function customize_theme_alert_bar(){ ?>
		<div id="alert-bar">
			<table class="form-table" role="presentation">
				<tbody>
					<tr>
						<th scope="row"><label for="enable_alert_bar"><?php _e('Enable Alert Bar', 'starter_basic_admin') ?></label></th>
						<td><input id="enable_alert_bar" name="enable_alert_bar" type="checkbox" value="1" <?php echo self::$enable_alert_bar ? 'checked' : '' ?> ></td>
					</tr>
					<tr>
						<th scope="row"><label for="google_analytics_code"><?php _e('Alert Bar Content', 'starter_basic_admin') ?></label></th>
						<td><?php
							$content = self::$alert_bar_content;
							$custom_editor_id = "alert_bar_content";
							$custom_editor_name = "alert_bar_content";
							$args = array(
								'media_buttons' => false,
								'textarea_name' => $custom_editor_name,
								'textarea_rows' => get_option('default_post_edit_rows', 10),
							);
							wp_editor($content, $custom_editor_id, $args);
							?>
						</td>
					</tr>
				</tbody>
			</table>
		</div>
	<?php
	}

	static function customize_theme_arbitrary_code_fields(){ ?>
		<div id="editor-tabs" class="tab-section">
			<ul class="category-tabs clearfix">
				<li><a href="#arbitrary_code_head"><?php _e('In the <code>&lt;head&gt;</code> tag', 'starter_basic_admin'); ?></a></li>
				<li><a href="#arbitrary_code_begin_body"><?php _e('After the opening <code>&lt;body&gt;</code> tag', 'starter_basic_admin'); ?></a></li>
				<li><a href="#arbitrary_code_end_body"><?php _e('Before the closing <code>&lt;body&gt;</code> tag', 'starter_basic_admin'); ?></a></li>
			</ul>
			<br class="clear" />
			<div id="arbitrary_code_head">
				<textarea id="acf-arbitrary_code_head_content" name="arbitrary_code_head"><?php echo self::$arbitrary_code_head ?></textarea>
			</div>
			<div class="hidden" id="arbitrary_code_begin_body">
				<textarea id="acf-arbitrary_code_begin_body_content" name="arbitrary_code_begin_body"><?php echo self::$arbitrary_code_begin_body ?></textarea>
			</div>
			<div class="hidden" id="arbitrary_code_end_body">
				<textarea id="acf-arbitrary_code_end_body_content" name="arbitrary_code_end_body"><?php echo self::$arbitrary_code_end_body ?></textarea>
			</div>
		</div>
	<?php
	}

	static function customize_theme_actions(){ ?>
		<input id="save-customize-theme" name="submit" class="button button-primary" type="submit" value="<?php _e('Save', 'starter_basic_admin'); ?>" />
	<?php
	}

	static function customize_company_logo(){
		wp_enqueue_media(); ?>
		<img id="customize-theme-image-preview" src="<?php echo wp_get_attachment_image_url(self::$logo) ?>" width="100" height="100" style="width: 100%; height: auto">
		<input id="upload-image-button" type="button" class="button" value="<?php _e( 'Upload image' ); ?>" />
		<input id="logo" name="logo" type="hidden" value="<?php echo self::$logo ?>">
	<?php
	}

	/**
	 * Undocumented function
	 *
	 * @return void
	 */
	static function render_customize_page(){
		do_action('add_meta_boxes', 'settings_customize_theme', ''); ?>
		<div class="wrap">
			<h1 class="wp-heading-inline"><?php _e('Customize Theme', 'starter_basic_admin') ?></h1>
			<hr class="wp-header-end">
			<form id="customize-theme-form" action="options-general.php?page=customize_theme" method="post">
				<?php wp_nonce_field('customize_theme_meta_box_nonce', 'customize_theme_meta_box_nonce'); ?>
				<div id="poststuff">
					<div id="post-body" class="metabox-holder columns-2">
						<div id="postbox-container-1" class="postbox-container">
							<?php do_meta_boxes('settings_customize_theme', 'side', null) ?>
						</div>
						<div id="postbox-container-2" class="postbox-container">
							<?php do_meta_boxes('settings_customize_theme', 'normal', null) ?>
						</div>
					</div>
				</div>
			</form>
		</div>
	<?php
	}

	/**
	 * Undocumented function
	 *
	 * @return void
	 */
	static function customize_theme_meta_box_save(){
		if(defined('DOING_AUTOSAVE') && DOING_AUTOSAVE){
			return;
		}

		if(!isset($_POST['data']['customize_theme_meta_box_nonce']) || !wp_verify_nonce($_POST['data']['customize_theme_meta_box_nonce'], 'customize_theme_meta_box_nonce')){
			return;
		}

		if(!current_user_can('activate_plugins') || !current_user_can('edit_theme_options')){
			return;
		}

		$data = $_POST['data'];
		unset($_POST);

		set_theme_mod('logo', $data['logo']);
		set_theme_mod('phone_number', $data['phone_number']);
		set_theme_mod('email', $data['email']);
		set_theme_mod('street_address_1', $data['street_address_1']);
		set_theme_mod('street_address_2', $data['street_address_2']);
		set_theme_mod('city', $data['city']);
		set_theme_mod('state', $data['state']);
		set_theme_mod('postal', $data['postal']);

		set_theme_mod('twitter_link', $data['twitter_link']);
		set_theme_mod('facebook_link', $data['facebook_link']);
		set_theme_mod('instagram_link', $data['instagram_link']);
		set_theme_mod('youtube_link', $data['youtube_link']);

		set_theme_mod('enable_analytics', $data['enable_analytics']);
		set_theme_mod('google_analytics_code', $data['google_analytics_code']);
		set_theme_mod('google_tags_code', $data['google_tags_code']);
		set_theme_mod('google_maps_code', $data['google_maps_code']);
		set_theme_mod('recaptcha_site_key', $data['recaptcha_site_key']);
		set_theme_mod('recaptcha_secret_key', $data['recaptcha_secret_key']);
		set_theme_mod('fb_pixel_code', $data['fb_pixel_code']);

		set_theme_mod('arbitrary_code_head', stripslashes($data['arbitrary_code_head']));
		set_theme_mod('arbitrary_code_begin_body', stripslashes($data['arbitrary_code_begin_body']));
		set_theme_mod('arbitrary_code_end_body', stripslashes($data['arbitrary_code_end_body']));

		set_theme_mod('enable_alert_bar', $data['enable_alert_bar']);
		set_theme_mod('alert_bar_content', stripslashes($data['alert_bar_content']));

		update_option('about-page-mapping', $data['about-page-mapping']);
		update_option('contact-page-mapping', $data['contact-page-mapping']);
		update_option('site-map-page-mapping', $data['site-map-page-mapping']);
	}
}