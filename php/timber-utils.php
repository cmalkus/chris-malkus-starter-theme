<?php
use Timber\FunctionWrapper;

/**
 * Add global options to the timber context. Currrently adds new menu and widget locations.
 * It also adds the sidebar. If you want to disable the sidebar globally, remove the '$data['sidebar']' line
 * @param Object $data The Timber context
 * @return Object The Timber context
 */
function add_to_context($data){
	global $current_user;
	$data['enable_alert_bar'] = boolval(get_theme_mod('enable_alert_bar'));
	$data['alert_message'] = apply_filters('the_content', stripslashes(get_theme_mod('alert_bar_content')));
	$data['main_menu'] = new TimberMenu('main_navigation');
	$data['footer_menu'] = new TimberMenu('footer_navigation');
	$data['breadcrumbs'] = Breadcrumbs::create('&#x221f;', get_the_title(get_option('page_on_front')));
	$data['can_login'] = class_exists('Auth');
	$data['is_logged_in'] = is_user_logged_in();

	if(function_exists('qtranxf_useCurrentLanguageIfNotFoundUseDefaultLanguage')){
		$data['available_languages'] = get_option('qtranslate_language_names');
	}

	if(is_user_logged_in()){
		$data['user'] = $current_user;
		$data['user_link'] = Members::member_permalink($current_user->data->user_nicename);
	}

	return $data;
}
add_filter('timber_context', 'add_to_context');

/**
 * Function for adding filters and functions to twig.
 * @param Object $twig The twig scope
 */
function add_to_twig($twig){
	$twig->getExtension('Twig_Extension_Core')->setTimezone(date_default_timezone_get() ?: 'UTC');
	$twig->addExtension(new Twig_Extension_StringLoader());

	$twig->addFilter(new Twig_SimpleFilter('get_webp', function ($url){
		if(isset($_SERVER['HTTP_USER_AGENT'])){
			$agent = $_SERVER['HTTP_USER_AGENT'];
		}

		if(strlen(strstr($agent, 'Chrome')) > 0){
			return str_ireplace(array('.jpg', '.jpeg', '.png', '.gif'), '.webp', $url);
		} else return $url;
	}));

	$twig->addFilter(new Twig_SimpleFilter('parse_url', function($url){
		if(!$url){
			return __('Empty string!', 'starter_basic');
		}

		return parse_url($url);
	}));

	$twig->addFilter(new Twig_SimpleFilter('preg_replace', function($subject, $pattern, $replacement){
		if(!$subject){
			return __('Empty string!', 'starter_basic');
		}

		return preg_replace($pattern, $replacement, $subject);
	}));

	$twig->addFilter(new Twig_SimpleFilter('remove_shortcodes', function($subject){
		if(!$subject){
			return __('Empty string!', 'starter_basic');
		}

		return preg_replace('~\[\/?.+?\]~', '', $subject);
	}));

	$twig->addFilter(new Twig_SimpleFilter('date_format', function($date = 'now', $date_format = ''){
		$df = $date_format ?: get_option('date_format');
		if(is_numeric($date)){
			$date = date('Y-m-d\\TH:i', $date);
		}
		$date_time = new DateTime($date, new DateTimeZone(get_option('timezone_string')));

		return $date_time->format($df);
	}));

	return $twig;
}
add_filter('get_twig', 'add_to_twig');