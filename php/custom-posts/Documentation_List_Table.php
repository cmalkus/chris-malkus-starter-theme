<?php
namespace Documentation;
use WP_List_Table;

if(!class_exists('WP_List_Table')){
	require_once( ABSPATH . 'wp-admin/includes/class-wp-list-table.php' );
}

if(!class_exists('WP_Posts_List_Table')){
	require_once( ABSPATH . 'wp-admin/includes/class-wp-posts-list-table.php' );
}

Class Documentation_List_Table extends \WP_Posts_List_Table{
	// remove search box
	public function search_box($text, $input_id){ }

	// Your custom list table is here
	public function display(){
		global $wp_query;

		function get_children($posts){
			if($posts->have_posts()){
				echo '<ul>';
				while($posts->have_posts()){
					$posts->the_post();
					$id = get_the_ID();
					echo '<li><a href="' . get_admin_url() . 'post.php?post=' . $id . '&action=edit">' . get_the_title() . '</a>';
					$children = new \WP_Query(
						array(
							'post_type' => get_post_type($id),
							'post_parent' => $id,
							'orderby' => 'menu_order',
						)
					);
					get_children($children);
					echo '</li>';
				}
				echo '</ul>';
			}

			wp_reset_query();
		}

		// error_log(print_r($wp_query, 1));
		$wp_query->query_vars['fields'] = '';
		$wp_query->query_vars['post_parent'] = 0;
		$docs = new \WP_Query($wp_query->query_vars);
		echo '<div id="documentation" class="postbox-container">';
		echo '<div class="postbox">';
		echo '<div class="inside">';
		get_children($docs);
		echo '</div>';
		echo '</div>';
		echo '</div>';
	}
}