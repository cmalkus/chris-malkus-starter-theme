<?php
namespace Documentation;
require_once get_template_directory() . '/php/custom-posts/Documentation_List_Table.php';

Documentation::init();

Class Documentation{
	private static $roles = array(
		// 'edit_post'              => "edit_documentation",
		// 'read_post'              => "read_documentation",
		// 'delete_post'            => "delete_documentation",

		// // 'edit_posts'             => "edit_documentations",
		// 'edit_others_posts'      => "edit_others_documentation",
		// 'publish_posts'          => "publish_documentation",
		// 'read_private_posts'     => "read_private_documentation",

		// 'read'                   => "read",
		// 'delete_posts'           => "delete_documentation",
		// 'delete_private_posts'   => "delete_private_documentation",
		// 'delete_published_posts' => "delete_published_documentation",
		// 'delete_others_posts'    => "delete_others_documentation",
		// 'edit_private_posts'     => "edit_private_documentation",
		// 'edit_published_posts'   => "edit_published_documentation",
		'create_posts'           => "create_documentation",
	);

	public function __construct(){}

	public static function init(){
		add_action('init', array(__CLASS__, 'create_post_type'));
		add_action('admin_init', array(__CLASS__, 'add_admin_roles'));
		add_action('init', array(__CLASS__, 'hide_editor'));
		add_filter('get_sample_permalink_html', array(__CLASS__, 'hide_permalinks'), 10, 5);
		add_filter('views_edit-documentation', array(__CLASS__, 'custom_list_table'));
		add_action('edit_form_after_title', array(__CLASS__, 'show_post_content'));
		add_action('do_meta_boxes', array(__CLASS__, 'hide_publish_metabox'));
		add_filter('screen_options_show_screen', array(__CLASS__, 'remove_screen_options'));
		add_action('load-edit.php', array(__CLASS__, 'disable_post_lock'), 0);
		add_action('load-post.php', array(__CLASS__, 'disable_post_lock'), 0);
		add_filter('show_post_locked_dialog', array(__CLASS__, 'close_dialog'), 99, 3);
		add_action('init', array(__CLASS__, 'stop_heartbeat'), 1 );
	}

	/**
	 * Create the post type for Forms.
	 */
	static function create_post_type(){
		register_post_type('documentation',
			array(
				'labels' => array(
					'name'          => __('Documentation', 'starter_basic_admin'),
					'singular_name' => __('Documentation', 'starter_basic_admin'),
					'archives'      => __('Documentation', 'starter_basic'),
					'menu_name'     => __('Documentation', 'starter_basic_admin'),
				),
				'capabilities'        => self::$roles,
				'supports'            => array('title', 'page-attributes', 'editor', 'disabled_post_lock'),
				'menu_icon'           => 'dashicons-media-text',
				'public'              => true,
				'has_archive'         => false,
				'exclude_from_search' => true,
				'hierarchical'        => true,
				'show_in_rest'        => true,
				'show_in_nav_menus'   => false,
				'publicly_queriable'  => true,
				'menu_position'       => 81,
				'rewrite'             => array('slug' => 'documentation'),
				'map_meta_cap'        => true
			)
		);
	}

	/**
	 * This hides the permalink from the gallery post type
	 * @param  Object $return    The default permalink
	 * @param  Number $post_id   The post ID
	 * @param  string $new_title Title of the post
	 * @param  string $new_slug  Post Slug
	 * @param  Object $post      The post itself
	 * @return Object            The default permalink or nothing, if its a gallery post type
	 */
	static function hide_permalinks($return, $post_id, $new_title, $new_slug, $post){
		if($post->post_type == 'documentation'){
			return '';
		}
		return $return;
	}

	static function add_admin_roles(){
		$role = get_role('administrator');

		foreach(self::$roles as $capability){
			$role->add_cap($capability);
		}
	}

	static function custom_list_table(){
		global $wp_list_table;

		if(!current_user_can('manage_options')){
			$list_table = new Documentation_List_Table();
			$wp_list_table = $list_table ;
		}
	}

	static function show_post_content(){
		$screen = get_current_screen();

		if($screen->post_type === 'documentation'){
			if(!current_user_can('manage_options')){
				wp_enqueue_style('wp-block-library');
				wp_enqueue_global_styles();

				echo '<style>.wrap h1.wp-heading-inline{display: none;}</style>';
				echo '<div class="postbox-container">';
				echo '<div class="postbox">';
				echo '<div class="inside">';
				the_content();
				echo '</div>';
				echo '</div>';
				echo '</div>';
			}
		}
	}

	static function hide_editor(){
		if(!isset($_GET['post'])) return;
		$post_type = get_post_type($_GET['post']);

		if($post_type === 'documentation'){
			if(!current_user_can('manage_options')){
				remove_post_type_support('documentation', 'editor');
				remove_post_type_support('documentation', 'page-attributes');
				remove_post_type_support('documentation', 'title');
			}
		}
	}

	static function hide_publish_metabox(){
		if(!current_user_can('manage_options')){
			remove_meta_box('submitdiv', 'documentation', 'side');
		}
	}

	static function remove_screen_options() {
		if(!current_user_can('manage_options')) {
			return false;
		}

		return true;
	}

	static function disable_post_lock(){
		if(post_type_supports(get_current_screen()->post_type, 'disabled_post_lock')){
			add_filter('wp_check_post_lock_window', '__return_false');
		}
	}

	function close_dialog($show, $post, $user){
		if($post->post_type == 'documentation'){
			return false;
		}

		return $show;
	}

	static function stop_heartbeat(){
		$post_type = isset($_GET['post_type']) ? $_GET['post_type'] : (isset($_GET['post']) ? get_post_type($_GET['post']) : null);

		if($post_type == 'documentation'){
			wp_deregister_script('heartbeat');
		}
	}
}