<?php
Form_Builder::init();

Class Form_Builder{
	const INPUT_TYPES = array(
		'text' => array(
			'label' => 'Text',
			'icon_class' => 'dashicons-editor-textcolor',
			'multi' => false,
		),
		'textarea' => array(
			'label' => 'Text Area',
			'icon_class' => 'dashicons-editor-alignleft',
			'multi' => false,
		),
		'number' => array(
			'label' => 'Number',
			'icon_class' => 'dashicons-calculator',
			'multi' => false,
		),
		'email' => array(
			'label' => 'Email',
			'icon_class' => 'dashicons-email',
			'multi' => false,
		),
		'tel' => array(
			'label' => 'Phone',
			'icon_class' => 'dashicons-phone',
			'multi' => false,
		),
		'date' => array(
			'label' => 'Date',
			'icon_class' => 'dashicons-calendar-alt',
			'multi' => false,
		),
		'file' => array(
			'label' => 'File',
			'icon_class' => 'dashicons-media-default',
			'multi' => false,
		),
		'select' => array(
			'label' => 'Select',
			'icon_class' => 'dashicons-editor-ul',
			'multi' => true,
		),
		'radio' => array(
			'label' => 'Radio',
			'icon_class' => 'dashicons-marker',
			'multi' => true,
		),
		'checkbox' => array(
			'label' => 'Checkbox',
			'icon_class' => 'dashicons-saved',
			'multi' => true,
		),
	);

	private static $captcha_site_key;
	private static $captcha_secret_key;
	private static $captcha_have_api_keys = 0;

	public function __construct(){}

	public static function init(){
		self::$captcha_site_key = get_theme_mod('recaptcha_site_key');
		self::$captcha_secret_key = get_theme_mod('recaptcha_secret_key');
		self::$captcha_have_api_keys = isset(self::$captcha_site_key) && !empty(self::$captcha_site_key) && isset(self::$captcha_secret_key) && !empty(self::$captcha_secret_key);

		add_action('init', array(__CLASS__, 'create_from_post_type'));
		add_action('init', array(__CLASS__, 'create_submission_post_type'));
		add_action('admin_menu', array(__CLASS__, 'add_submissions_menu'));
		add_action('add_meta_boxes', array(__CLASS__, 'add_meta_boxes'), 1);
		add_action('save_post', array(__CLASS__, 'repeatable_meta_box_save'));
		add_action('manage_form_submission_posts_custom_column', array(__CLASS__, 'form_submission_table_content'), 10, 2);
		add_action('load-post.php', array(__CLASS__, 'mark_submission_as_read'));
		add_action('manage_posts_extra_tablenav', array(__CLASS__, 'download_submission_csv_button'));
		add_action('wp_ajax_download_submission_csv', array(__CLASS__, 'download_submission_csv'));
		add_action('wp_ajax_nopriv_submit_form_builder', array(__CLASS__, 'submit_form_builder'));
		add_action('wp_ajax_submit_form_builder', array(__CLASS__, 'submit_form_builder'));
		add_action('restrict_manage_posts', array(__CLASS__, 'form_submission_table_filtering'));
		add_filter('parse_query', array(__CLASS__, 'form_submission_table_filter'));
		add_filter('manage_edit-form_submission_sortable_columns', array(__CLASS__, 'form_submission_table_sorting'));
		add_filter('request', array(__CLASS__, 'form_name_column_orderby'));
		add_filter('manage_form_submission_posts_columns', array(__CLASS__, 'form_submission_table_head'));
		add_filter('get_sample_permalink_html', array(__CLASS__, 'hide_permalinks'), 10, 5);
		add_filter('display_post_states', array(__CLASS__, 'filter_display_post_states'), 10, 2);
		add_shortcode('form', array(__CLASS__, 'create_shortcode'));
	}

	/**
	 * Create the post type for Forms.
	 */
	static function create_from_post_type(){
		register_post_type('form',
			array(
				'labels' => array(
					'name' => __('Forms', 'starter_basic_admin'),
					'singular_name' => __('Form', 'starter_basic_admin'),
					'archives' => __('Forms', 'starter_basic'),
					'menu_name' => __('Forms', 'starter_basic_admin'),
				),
				'supports' => array('title', 'page-attributes'),
				'menu_icon' => 'dashicons-welcome-write-blog',
				'public' => true,
				'has_archive' => false,
				'exclude_from_search' => true,
				'hierarchical' => false,
				'show_in_nav_menus' => false,
				'publicly_queriable' => true,
				'rewrite' => array('slug' => 'forms'),
			)
		);
	}

	/**
	 * Create the post type for Form submissions.
	 */
	static function create_submission_post_type(){
		register_post_type('form_submission',
			array(
				'labels' => array(
					'name' => __('Form Submissions', 'starter_basic_admin'),
					'singular_name' => __('Form Submission', 'starter_basic_admin'),
					'archives' => __('Form Submissions', 'starter_basic'),
					'menu_name' => __('Form Submissions', 'starter_basic_admin'),
				),
				'supports' => array('title', 'page-attributes'),
				'menu_icon' => 'dashicons-editor-ol-rtl',
				'public' => true,
				'exclude_from_search' => false,
				'publicly_queryable'  => false,
				// 'show_ui'             => false,
				'show_in_nav_menus'   => false,
				'show_in_menu'        => false,
				'show_in_admin_bar'   => false,
				'has_archive'         => false,
				'rewrite' => array('slug' => 'forms'),
				'capabilities' => array(
					'create_posts' => 'do_not_allow',
					// 'edit_posts' => 'do_not_allow',
				),
				'map_meta_cap' => true,
			)
		);
	}

	/**
	 * Undocumented function
	 */
	static function add_submissions_menu(){
		add_submenu_page('edit.php?post_type=form', __('Form Submissions', 'starter_basic_admin'), __('Form Submissions', 'starter_basic_admin'), 'activate_plugins', 'edit.php?post_type=form_submission');
	}

	/**
	 * This hides the permalink from the gallery post type
	 * @param  Object $return    The default permalink
	 * @param  Number $post_id   The post ID
	 * @param  string $new_title Title of the post
	 * @param  string $new_slug  Post Slug
	 * @param  Object $post      The post itself
	 * @return Object            The default permalink or nothing, if its a gallery post type
	 */
	static function hide_permalinks($return, $post_id, $new_title, $new_slug, $post){
		if($post->post_type == 'form' || $post->post_type == 'form_submission'){
			return '';
		}
		return $return;
	}

	static function add_meta_boxes(){
		add_meta_box('form-fields-meta-box', __('Fields', 'starter_basic_admin'), array(__CLASS__, 'repeatable_meta_box'), 'form', 'normal', 'default');
		add_meta_box('form-shortcode-meta-box', __('Usage', 'starter_basic_admin'), array(__CLASS__, 'display_shortcode'), 'form', 'side', 'default');

		add_meta_box('form_submission-meta-box', __('Submission Data', 'starter_basic_admin'), array(__CLASS__, 'form_data_meta_box'), 'form_submission', 'normal', 'default');
	}

	/**
	 * Just spits out copyable text about how to use the gallery
	 */
	static function display_shortcode(){
		global $post; ?>
		<div id="form-usage">
			<p><?php _e('Add the following to post:', 'starter_basic_admin');?></p>
			<pre>[form id="<?php echo $post->ID ?>"]</pre>
		</div>
	<?php }

	/**
	 * This is the front end for the gallery fields. Here is where we enqueue the style and scripts to make the repeater run.
	 * The image props are kinda janky. They are saved as a php object, but need to be converted to JSON on the front end becuse using a php object in a text field destroys its usability
	 */
	static function repeatable_meta_box(){
		global $post;
		$form_fields = get_post_meta($post->ID, 'form_fields', true);
		wp_nonce_field('from_fields_meta_box_nonce', 'from_fields_meta_box_nonce');
		require_once get_template_directory() . '/php/partials/form-fields-view.php';
	}

	/**
	 * This saves our fields with the post.
	 * The image props are kinda janky though. They are saved as a php object, but need to be converted to JSON on the front end
	 * becuse using a php object in a text field destroys its usability
	 * @param  int $post_id The post we're working with
	 */
	static function repeatable_meta_box_save($post_id){
		if(!isset($_POST['from_fields_meta_box_nonce']) || !wp_verify_nonce($_POST['from_fields_meta_box_nonce'], 'from_fields_meta_box_nonce')){
			return;
		}

		if(defined('DOING_AUTOSAVE') && DOING_AUTOSAVE){
			return;
		}

		if(!current_user_can('edit_post', $post_id)){
			return;
		}

		$old = get_post_meta($post_id, 'form_fields', true);
		$new = array();

		$labels = $_POST['label'];
		$placeholders = $_POST['placeholder'];
		$types = $_POST['type'];
		$requireds = $_POST['required'];
		$widths = $_POST['width'];
		$multi_options = $_POST['multi'];
		$count = count($labels);

		//die($count);

		for ($i = 0; $i < $count; $i++){
			$new[$i]['label'] = stripslashes(strip_tags($labels[$i]));
			$new[$i]['placeholder'] = stripslashes(strip_tags($placeholders[$i]));
			$new[$i]['type'] = stripslashes(strip_tags($types[$i]));
			$new[$i]['required'] = stripslashes(strip_tags($requireds[$i]));
			$new[$i]['width'] = stripslashes(strip_tags($widths[$i]));
			$new[$i]['options'] = isset($multi_options['input_' . $i]) ? $multi_options['input_' . $i] : '';
		}

		// error_log(print_r($new, 1));
		if(!empty($new) && $new != $old){
			update_post_meta($post_id, 'form_fields', $new);
		} elseif(empty($new) && $old){
			delete_post_meta($post_id, 'form_fields', $old);
		}

		update_post_meta($post_id, 'make_post', $_POST['make_post']);
		update_post_meta($post_id, 'send_email', $_POST['send_email']);
		update_post_meta($post_id, 'to_emails', $_POST['to_emails']);
	}

	/**
	 * Function for the form shortcode. shortcode is formatted like this: [form id="XXX" type="{content/links}"]
	 * @param  [array] $atts the attributes for the shortcode. takes id and type. id must be a valid form id and type must be either "content" or "links"
	 * @return [string]      html string of the forms
	 */
	function create_shortcode($atts) {
		if(self::$captcha_have_api_keys){
			wp_enqueue_script('google_recaptcha', 'https://www.google.com/recaptcha/api.js');
		}

		$out = '';
		$a = shortcode_atts(array(
			'id' => '0',
		), $atts);

		if(get_post_type($a['id']) !== 'form'){
			return _e('Error: Post is not an form', 'starter_basic');
		}

		$form = get_post($a['id']);
		$fields = get_post_meta($form->ID, 'form_fields')[0];
		$out .= '<form id="form-' . $form->ID . '" class="form-builder row" method="post" action="." enctype="multipart/form-data">';
		foreach($fields as $key => $field){
			$width = round(intval($field['width']) / 100 * 12);
			$out .= '<div class="col-sm-12 col-lg-' . $width .'">';

			switch($field['type']){
				case 'textarea':
					$out .= '<div class="input-wrap">';
					$out .= '<textarea id="' . strtolower(str_replace(' ', '_', $field['label'])) . '" name="' . strtolower(str_replace(' ', '_', $field['label'])) . '" placeholder="' . $field['placeholder'] . '" ' . (filter_var($field['required'], FILTER_VALIDATE_BOOLEAN) ? 'required' : '') . '></textarea>';
					$out .= '<label for="' . strtolower(str_replace(' ', '_', $field['label'])) . '">' . $field['label'] . '</label>';
					$out .= '<span></span>';
					$out .= '</div>';
				break;
				case 'select':
					$out .= '<label for="' . strtolower(str_replace(' ', '_', $field['label'])) . '">' . $field['label'] . '</label>';
					$out .= '<select id="' . strtolower(str_replace(' ', '_', $field['label'])) . '" name="' . strtolower(str_replace(' ', '_', $field['label'])) . '" ' . (filter_var($field['required'], FILTER_VALIDATE_BOOLEAN) ? 'required' : '') . '>';
					$out .= '<option selected="true" disabled="disabled" value="">' . __('Choose&hellip;', 'starter_basic') . '</option>';
					foreach($field['options'] as $option){
						$out .= '<option value="' . $option . '">' . $option . '</option>';
					}
					$out .= '</select>';
				break;
				case 'radio':
				case 'checkbox':
					$out .= '<div>' . $field['label'] . '</div>';
					foreach($field['options'] as $option){
						$out .= '<label><input type="' . $field['type'] . '" name="' . strtolower(str_replace(' ', '_', $field['label'])) . '" value="' . $option . '" ' . (filter_var($field['required'], FILTER_VALIDATE_BOOLEAN) ? 'required' : '') . '>' . $option . '</label>';
					}
				break;
				case 'file':
					$out .= '<div class="input-wrap">';
					$out .= '<label for="' . strtolower(str_replace(' ', '_', $field['label'])) . '">' . $field['label'] . '</label>';
					$out .= '<input id="' . strtolower(str_replace(' ', '_', $field['label'])) . '" type="' . $field['type'] . '" placeholder="' . $field['placeholder'] . '" name="' . strtolower(str_replace(' ', '_', $field['label'])) . '" ' . (filter_var($field['required'], FILTER_VALIDATE_BOOLEAN) ? 'required' : '') . ' accept=".doc,.docx,.odt,.jpg,.jpeg,.png,.gif,.pdf,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document,application/vnd.oasis.opendocument.text,image/*,application/pdf">';
					$out .= '<span></span>';
					$out .= '</div>';
				break;
				default:
					$out .= '<div class="input-wrap">';
					$out .= '<input id="' . strtolower(str_replace(' ', '_', $field['label'])) . '" type="' . $field['type'] . '" placeholder="' . $field['placeholder'] . '" name="' . strtolower(str_replace(' ', '_', $field['label'])) . '" ' . (filter_var($field['required'], FILTER_VALIDATE_BOOLEAN) ? 'required' : '') . '>';
					$out .= '<label for="' . strtolower(str_replace(' ', '_', $field['label'])) . '">' . $field['label'] . '</label>';
					$out .= '<span></span>';
					$out .= '</div>';
				break;
			}

			$out .= '</div>';
		}
		$out .= '<input type="hidden" name="form_id" value="' . $form->ID . '">';
		if(self::$captcha_have_api_keys){
			$out .= '<div class="col-xs-12"><div class="g-recaptcha" data-width="100" data-sitekey="' . self::$captcha_site_key . '"></div></div>';
		}
		$out .= '<div class="col-xs mt-4"><button class="btn btn-primary" type="submit">' . __('Submit', 'starter_basic') .'</button></div>';
		$out .= '</form>';
		return $out;
	}

	function submit_form_builder(){
		$response = new stdClass();
		$response->errors = [];
		$response->status = 0;

		if(isset($_POST['form_id']) && !empty($_POST['form_id']) && $_POST['form_id'] !== ''){
			$form = get_post($_POST['form_id']);

			if(self::$captcha_have_api_keys && $_POST['g-recaptcha-response']){
				$captcha = $_POST['g-recaptcha-response'];
				$user_ip = $_SERVER['REMOTE_ADDR'];

				$captcha_response = file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret=' . self::$captcha_secret_key . '&response=' . $captcha . '&remoteip=' . $user_ip);
				$response_keys = json_decode($captcha_response, true);
				unset($_POST['g-recaptcha-response']);
			}

			unset($_POST['form_id']);
			unset($_POST['action']);
			$fields = $_POST;
			$files = $_FILES;
			$_POST = null;
			$_FILES = null;

			if(intval($response_keys['success']) === 1 || !self::$captcha_have_api_keys){
				if(get_post_type($form) === 'form'){
					$make_post = filter_var(get_post_meta($form->ID, 'make_post')[0], FILTER_VALIDATE_BOOLEAN);
					$send_email = filter_var(get_post_meta($form->ID, 'send_email')[0], FILTER_VALIDATE_BOOLEAN);

					if(count($fields) >= 1 || count($files) >= 1){
						if(isset($make_post) && !empty($make_post) && boolval($make_post)){
							$post = array(
								'post_title' => $form->post_title . ': ' . date('m/d/y - G:i:s'),
								'post_status' => 'publish',
								'post_type' => 'form_submission',
							);

							$new_post = wp_insert_post($post);

							if(count($files) >= 1){
								foreach($files as $key => $file){
									$filename = basename($file['tmp_name']);
									$upload_file = wp_upload_bits($file['name'], null, file_get_contents($file['tmp_name']));

									if (!$upload_file['error']) {
										$wp_filetype = wp_check_filetype($filename, null );
										$attachment = array(
											'post_mime_type' => $wp_filetype['type'],
											'post_title' => preg_replace('/\.[^.]+$/', '', $file['name']),
											'post_content' => '',
											'post_status' => 'inherit'
										);
										$attachment_id = wp_insert_attachment( $attachment, $upload_file['file'] );
										if(!is_wp_error($attachment_id)) {
											require_once(ABSPATH . "wp-admin" . '/includes/image.php');
											$attachment_data = wp_generate_attachment_metadata( $attachment_id, $upload_file['file'] );
											wp_update_attachment_metadata( $attachment_id,  $attachment_data );

											$fields[$key] = ('<a href="' . wp_get_attachment_url($attachment_id) . '">' . $file['name'] . '</a>');
										}else{
											$response->errors[] = __('Failed to create file post', 'starter_basic');
										}
									}else{
										$response->errors[] = $upload_file['error'];
									}
								}
							}

							update_post_meta($new_post, 'form_submission_data', $fields);
							update_post_meta($new_post, 'form_id', $form->ID);
							update_post_meta($new_post, 'unread', true);
						}

						if(isset($send_email) && !empty($send_email) && boolval($send_email)){
							$to_emails = array_map('trim', explode(',', get_post_meta($form->ID, 'to_emails')[0]));

							if(isset($to_emails) && !empty($to_emails)){
								$headers = "Content-Type: text/plain; charset=UTF-8\r\n" . 'From: ' . get_bloginfo('name') . ' <' . get_theme_mod('email') ?: 'webmaster@' . str_replace(array('http://', 'https://'), '', get_site_url()) . '>';
								$message = sprintf(__('New submission from form "%s"', 'starter_basic_admin'), $form->post_title) . "\r\n\r\n";

								if(count($files) >= 1){
									$attachments = array();
									if(!file_exists(get_template_directory() . '/tmp/')){
										wp_mkdir_p(get_template_directory() . '/tmp/');
									}
									foreach($files as $key => $file){
										if($file['size'] > 5000000){
											$response->errors[] = sprintf(__('File "%s" is too large', 'starter_basic'), $file['name']);
											continue;
										}

										file_put_contents(get_template_directory() . '/tmp/' . $file['name'], file_get_contents($file['tmp_name']));
										$attachments[] = get_template_directory() . '/tmp/' . $file['name'];
										$fields[$key] = $file['name'];
									}
								}

								foreach($fields as $key => $val){
									if(is_array($val)){
										$val = implode(', ', $val);
									}

									$label = ucwords(str_replace('_', ' ', $key));
									$message .= "$label: $val\r\n";
								}
								if(!wp_mail($to_emails, sprintf(__('New submission from form "%s"', 'starter_basic_admin'), $form->post_title), $message, $headers, $attachments)){
									$response->errors[] = __('Error sending email. Please contact site admin', 'starter_basic');
								}

								if(count($files) >= 1){
									foreach($files as $key => $file){
										unlink(get_template_directory() . '/tmp/' . $file['name']);
									}
								}
							}else{
								$response->errors[] = __('No email addresses set', 'starter_basic');
							}
						}
					}else{
						$response->errors[] = __('No form fields', 'starter_basic');
					}
				}else{
					$response->errors[] = __('Form ID is not actually a form', 'starter_basic');
				}
			}else{
				$response->errors[] = __('Captcha Failed', 'starter_basic');
			}
		}else{
			$response->errors[] = __('No form ID in form data', 'starter_basic');
		}

		if(empty($response->errors)){
			$response->status = 1;
		} else {
			$response->status = -count($response->errors);
		}
		header('Content-Type:application/json;');
		echo json_encode($response);
		wp_die();
	}

	function form_data_meta_box(){
		global $post;
		$form_submission_data = get_post_meta($post->ID, 'form_submission_data', true); ?>
		<div id="form_submission-data">
			<?php
				foreach($form_submission_data as $key => $val):
					if(is_array($val)){
						$val = implode(', ', $val);
					}

					$label = ucwords(str_replace('_', ' ', $key)); ?>
					<dl>
						<dt><?php echo $label; ?>:</dt>
						<dd><?php echo $val; ?></dd>
					</dl>
				<?php endforeach;
			?>
		</div>
		<?php
	}

	/**
	 * Adds the import calendar button on events page
	 * @param  string $screen the name of the current wp admin view
	 */
	static function download_submission_csv_button($screen){
		$screen = get_current_screen();
		if($screen->post_type == 'form_submission'){
			echo '<button class="submissions-download-csv button button-primary" type="button" title="Downlaod CSV">' . __('Downlaod CSV', 'starter_basic_admin') . '</button>';
		}
	}

	function download_submission_csv(){
		$form_post_id = $_POST['data']['form_post_id'];
		$orderby = $_POST['data']['orderby'];
		$order = $_POST['data']['order'];

		$args = array(
			'post_type' => 'form_submission',
			'number_posts' => -1,
			'posts_per_page' => -1,
		);

		if($form_post_id === 'all'){
			$args['meta_query'][] = array();
		}else if(is_numeric($form_post_id)){
			$args['meta_query'][] = array(
				'key' => 'form_id',
				'value' => $form_post_id,
				'compare' => '=',
			);
		}

		if(!empty($orderby) AND $orderby == 'from_name'){
			$args['orderby'] = 'meta_value';
			$args['meta_key'] = 'from_name';
			$args['order'] = strtoupper($order);
		}

		$grouped_posts = array();
		$posts = get_posts($args);

		foreach($posts as $post){
			$form_id = get_post_meta($post->ID, 'form_id')[0];
			$form = get_post($form_id);
			$form_slug = $form->post_name;
			$form_data = get_post_meta($post->ID, 'form_submission_data', true);

			$grouped_posts[$form_slug]['form_id'] = $form_id;
			$grouped_posts[$form_slug]['form_name'] = $form_slug;
			$grouped_posts[$form_slug]['submissions'][] = $form_data;
		}

		$out_files = array();

		foreach($grouped_posts as $group){
			$file = fopen('php://memory', 'w');
			$col_headers = array();

			foreach ($group['submissions'][0] as $key => $val){
				$col_headers[] = $key;
			}

			fputcsv($file, $col_headers);

			foreach ($group['submissions'] as $fields) {
				fputcsv($file, $fields);
			}

			fseek($file, 0);

			$out_files[] = $file;
		}

		if(count($out_files) === 1){
			$item = reset($grouped_posts);
			header('Content-Description: File Transfer');
			header('Content-Encoding: UTF-8');
			header("Content-Transfer-Encoding: binary");
			header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
			header('Pragma: public');
			header('Content-Type: application/zip');
			header('Content-Disposition: attachment; filename="form-data.zip"');
			header('Content-Length: ' . filesize($out_files[0]));
			header('Filename: ' . $item['form_name'] . '.csv');
			$content = stream_get_contents($out_files[0]);
			echo $content;
			fclose($out_files[0]);
			die();
		}else if(count($out_files) > 1){
			if(!file_exists(get_template_directory() . '/tmp/')){
				wp_mkdir_p(get_template_directory() . '/tmp/');
			}

			$id = uniqid();
			$zip = new ZipArchive();
			$zip_location = get_template_directory() . "/tmp/tmp_zip-$id.zip";
			$zip->open($zip_location, ZipArchive::CREATE);

			$grouped_keys = array_keys($grouped_posts);
			foreach($out_files as $i => $csv_file){
				file_put_contents('./' . $grouped_keys[$i] . '.csv', $csv_file);
				fclose($csv_file);
				$zip->addFile('./' . $grouped_keys[$i] . '.csv');
			}

			$zip->close();

			foreach($out_files as $i => $csv_file){
				unlink('./' . $grouped_keys[$i] . '.csv');
			}

			header('Content-Description: File Transfer');
			header('Content-Encoding: UTF-8');
			header("Content-Transfer-Encoding: binary");
			header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
			header('Pragma: public');
			header('Content-Type: application/zip');
			header('Content-Disposition: attachment; filename="form-data.zip"');
			header('Content-Length: ' . filesize($zip_location));
			header('Filename: form-data.zip');

			readfile($zip_location);
			unlink($zip_location);
			die();
		}
	}

	/**
	 * Adds the start date column
	 * @param array $defaults the default wp post table headers
	 * @return array the new table headers
	 */
	function form_submission_table_head($defaults){
		$new = array();

		foreach($defaults as $key => $title){
			$new[$key] = $title;
			if($key == 'title'){
				$new['form_name'] = __('Form', 'starter_basic_admin');
			}
		}
		return $new;
	}

	/**
	 * Populates the start date column
	 * @param  string $column_name Name of the column
	 * @param  int $post_id     the post id
	 */
	function form_submission_table_content($column_name, $post_id){
		if($column_name == 'form_name'){
			if(get_post_meta($post_id, 'form_id', true) != ''){
				$form_id = get_post_meta($post_id, 'form_id', true);
				echo get_post($form_id)->post_title;
			} else {
				_e('No Form', 'starter_basic_admin');
			}
		}
	}

	/**
	 * Makes the start date column sortable
	 * @param  array $columns The posts column headers
	 * @return array          The posts column headers
	 */
	function form_submission_table_sorting($columns){
		$columns['start_date'] = 'start_date';
		return $columns;
	}

	/**
	 * Orderes the evenst by date when you click it in the menu
	 * @param  array $vars the url query vars
	 * @return array       the modified url query vars
	 */
	static function form_name_column_orderby($vars){
		global $wp_query;

		if(isset($vars['orderby']) && 'form_name' == $vars['orderby']){
			$vars = array_merge($vars, array(
				'meta_key' => 'form_id',
				'orderby' => 'meta_value',
			));
		}

		return $vars;
	}

	/**
	 * Adds the droptdown filter
	 * @param  string $screen the name of the current wp admin view
	 */
	static function form_submission_table_filtering($screen){
		$args = array(
			'post_type' => 'form',
		);

		$forms = get_posts($args);
		if($screen == 'form_submission'){ ?>
			<select name="form_post_id">
				<option value="all" <?php if($_GET['form_post_id'] == 'all'): ?>selected="selected"<?php endif ?>><?php _e('All Submissions', 'starter_basic_admin') ?></option>
				<?php foreach($forms as $form) : ?>
					<option value="<?php echo $form->ID; ?>" <?php if($_GET['form_post_id'] == $form->ID): ?>selected="selected"<?php endif; ?>><?php echo $form->post_title; ?></option>
				<?php endforeach; ?>
			</select>
			<?php
		}
	}

	/**
	 * This is for the dropdown filter. It shows upcoming, past or all Events.
	 * @param array $query the url query vars
	 * TODO right now upcoming events shows all repeating events as well, even if they are done. Need to write in some logic to check if its over
	 */
	static function form_submission_table_filter($query){
		if(is_admin() && $query->query['post_type'] == 'form_submission' && $query->is_main_query()){
			$qv = &$query->query_vars;
			$qv['meta_query'] = array();

			if($_GET['form_post_id'] === 'all'){
				$qv['meta_query'][] = array();
			}else if(is_numeric($_GET['form_post_id'])){
				$qv['meta_query'][] = array(
					'key' => 'form_id',
					'value' => $_GET['form_post_id'],
					'compare' => '=',
				);
			}

			if(!empty($_GET['orderby']) AND $_GET['orderby'] == 'from_name'){
				$qv['orderby'] = 'meta_value';
				$qv['meta_key'] = 'from_name';
				$qv['order'] = strtoupper($_GET['order']);
			}
		}
	}

	/**
	 * Hook into page_states to add the content after the page name in the pages list
	 * @param  [array] $post_states array of post states. like "draft" or "posts page"
	 * @param  [stdObj] $post        the global $post
	 * @return [array]              our modified array
	 */
	static function filter_display_post_states($post_states, $post){
		if('page' === get_option('show_on_front')){
			if(get_post_meta($post->ID, 'unread', true)){
				$post_states['unread-submission'] = __('New', 'starter_basic_admin');
			}
		}

		return $post_states;
	}

	static function mark_submission_as_read(){
		if(!isset($_GET['post'])) return;
		$post_id = $_GET['post'];

		update_post_meta($post_id, 'unread', false);
	}
}
