<?php

Post_Archive_Options::init();

Class Post_Archive_Options{
	private static $roles = array(
		// 'edit_post'              => "edit_post_archive_options",
		// 'read_post'              => "read_documentation",
		// 'delete_post'            => "delete_documentation",

		// 'edit_posts'             => "edit_create_post_archive_options",
		// 'edit_others_posts'      => "edit_others_documentation",
		// 'publish_posts'          => "publish_documentation",
		// 'read_private_posts'     => "read_private_documentation",

		// 'read'                   => "read",
		// 'delete_posts'           => "delete_documentation",
		// 'delete_private_posts'   => "delete_private_documentation",
		// 'delete_published_posts' => "delete_published_documentation",
		// 'delete_others_posts'    => "delete_others_documentation",
		// 'edit_private_posts'     => "edit_private_documentation",
		// 'edit_published_posts'   => "edit_published_post_archive_options",
		'create_posts'           => "create_post_archive_options",
	);

	public function __construct(){}

	public static function init(){
		add_action('init', array(__CLASS__, 'create_post_type'));
	}

	/**
	 * Create the post type for FAQs.
	 */
	static function create_post_type(){
		register_post_type('post-archive-options',
			array(
				'labels' => array(
					'name' => __('Post Archive Options', 'starter_basic_admin'),
					'singular_name' => __('Post Archive Options', 'starter_basic_admin'),
					'menu_name' => __('Post Archive Options', 'starter_basic_admin'),
				),
				'capabilities'        => self::$roles,
				'hierarchical'        => true,
				'supports'            => array('title', 'editor', 'page-attributes'),
				'show_in_rest'        => true,
				'public'              => true,
				'exclude_from_search' => false,
				'publicly_queryable'  => false,
				// 'show_ui'             => false,
				'show_in_nav_menus'   => false,
				'show_in_menu'        => false,
				'show_in_admin_bar'   => false,
				'has_archive'         => false,
				'map_meta_cap'        => true,
			)
		);
	}
}
