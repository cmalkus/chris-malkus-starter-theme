<?php
/**
 * Section Block Template
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during backend preview render.
 * @param   int $post_id The post ID the block is rendering content against.
 *          This is either the post ID currently being displayed inside a query loop,
 *          or the post ID of the post hosting this block.
 * @param   array $context The context provided to the block by the post or it's parent block.
 */

$timber_context['block']   = $block;
$timber_context['content'] = $content;
$timber_context['preview'] = $is_preview;
$timber_context['context'] = $context;
$timber_context['post_id'] = $post_id;

$bg_image = get_field('section_bg_image');
if(isset($bg_image) && !empty($bg_image)) $timber_context['block']['style']['image']['background'] = $bg_image;

$class_list = array(
	isset($block['className']) ? $block['className'] : null,
	$is_preview ? 'section-preview' : null,
);

$timber_context['block']['className'] = implode(' ', array_filter($class_list));
// Block_Utils::geterate_style_sheet($block);

// error_log(print_r($timber_context, 1));
Timber::render('section.twig', $timber_context);