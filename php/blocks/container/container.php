<?php
/**
 * Container Block Template
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during backend preview render.
 * @param   int $post_id The post ID the block is rendering content against.
 *          This is either the post ID currently being displayed inside a query loop,
 *          or the post ID of the post hosting this block.
 * @param   array $context The context provided to the block by the post or it's parent block.
 */

$timber_context['block']   = $block;
$timber_context['content'] = $content;
$timber_context['preview'] = $is_preview;
$timber_context['context'] = $context;
$timber_context['post_id'] = $post_id;

switch($timber_context['block']['align']){
	case 'right':
		$container_class = 'container right-bleed';
	break;
	case 'left':
		$container_class = 'container left-bleed';
	break;
	case 'center':
		$container_class = 'container';
	break;
	default:
		$container_class = 'container-fluid';
	break;
}

$class_list = array(
	isset($block['className']) ? $block['className'] : null,
	$container_class,
	$is_preview ? 'container-preview' : null,
);

$timber_context['block']['className'] = implode(' ', array_filter($class_list));
// Block_Utils::geterate_style_sheet($block);

// error_log(print_r($block, 1));
Timber::render('container.twig', $timber_context);