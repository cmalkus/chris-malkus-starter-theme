<?php

return array(
	'finn/your-pattern' => array(
		'title'       => __('Your pattern title', 'starter_basic'),
		'description' => __('Your pattern description', 'starter_basic'),
		'content'     => "<!-- wp:paragraph -->\n<p>Your pattern</p>\n<!-- /wp:paragraph -->",
		'categories'  => array('section'),
	),
	'finn/section-container' => array(
		'title'       => __('Section + Container', 'starter_basic'),
		'description' => __('Section + Container', 'starter_basic'),
		'content'     => '<!-- wp:finn/section {"responsiveStyle":{"desktop":{"color":{"text":"#ffffff"},"spacing":{"padding":{"top":"12em","bottom":"12em"}}},"tablet":{"color":{"text":"#ffffff"},"spacing":{"padding":{"top":"10em","bottom":"10em"}}},"mobile":{"color":{"text":"#ffffff"},"spacing":{"padding":{"top":"8em","bottom":"8em"}}}},"name":"finn/section","data":{"section_bg_image":179,"_section_bg_image":"section_bg_image"},"mode":"preview","style":{"color":{"text":"#ffffff"},"spacing":{"padding":{"top":"12em","bottom":"12em"}}}} -->
		<!-- wp:finn/container {"responsiveStyle":{"tablet":[],"mobile":[]},"name":"finn/container","mode":"preview"} -->
		<!-- /wp:finn/container -->
		<!-- /wp:finn/section -->',
		'categories'  => array('section'),
	),
);
