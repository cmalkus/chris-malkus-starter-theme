<?php
/**
 * Load scripts and css specific to the admin
 */
function admin_load_files(){ ?>
	<script type="text/javascript" id="script-admin-vars">
		var templateUrl = '<?php echo get_stylesheet_directory_uri(); ?>',
			siteUrl = '<?php echo get_site_url(); ?>',
			postId = <?php global $post; echo (isset($post->ID)) ? $post->ID : 0; ?>,
			repeating, no_location;
			scriptAdminVars = document.getElementById('script-admin-vars');

		scriptAdminVars.parentNode.removeChild(scriptAdminVars);
	</script>
	<?php

	/**
	 * Load text domain for admin translations
	 */
	$mo_path = get_template_directory() . '/languages/';
	if(file_exists($mo_path . get_locale() . '_admin.mo')){
		load_textdomain('starter_basic_admin', $mo_path . get_locale() . '_admin.mo');
	}

	$block_deps = include get_stylesheet_directory() . '/js/app/blocks/build/blocks.jsx.asset.php';

	wp_enqueue_media();
	wp_enqueue_script('jquery-ui-core');
	wp_enqueue_script('jquery-ui-draggable');
	wp_enqueue_script('jquery-ui-tabs');
	wp_enqueue_script('google_maps', '//maps.googleapis.com/maps/api/js?key=AIzaSyDUmWrI564wWhoAnJmync64ZZPOHAYe1Ac');
	wp_enqueue_script('ace-editor', get_stylesheet_directory_uri() . '/js/lib/ace/ace.js', array('jquery-core'), '1.4.12', true);
	wp_enqueue_script('babel_polyfill', get_stylesheet_directory_uri() . '/node_modules/@wordpress/babel-preset-default/build/polyfill.min.js');
	wp_enqueue_script('admin_js', get_stylesheet_directory_uri() . '/js/app/admin.js');
	wp_enqueue_script('blocks_js', get_stylesheet_directory_uri() . '/js/app/blocks/build/blocks.jsx.js', $block_deps['dependencies']);
	wp_enqueue_style('admin_css', get_stylesheet_directory_uri() . '/css/admin/admin.css');

	// add_filter('script_loader_tag', function($tag, $handle, $src) {
	// 	// if not your script, do nothing and return original $tag
	// 	if ('blocks_js' !== $handle){
	// 		return $tag;
	// 	}
	// 	// change the script tag by adding type="module" and return it.
	// 	$tag = '<script type="module" src="' . esc_url($src) . '"></script>';
	// 	return $tag;
	// } , 10, 3);
}
add_action('admin_enqueue_scripts', 'admin_load_files');


/**
 * This adds custom fields to the theme customization. This is for social media, contact options, and the google analytics hook ID
 * @param  Object $wp_customize The WP customize object that we are adding to.
 */
function starter_basic_customize_register($wp_customize){
	$wp_customize->add_setting('single_unit_info', array(
		'default' => '',
		'transport' => 'refresh',
	));

	$wp_customize->add_setting('all_unit_info', array(
		'default' => '',
		'transport' => 'refresh',
	));
}
add_action('customize_register', 'starter_basic_customize_register');

/**
 * enables additional mime types to be uploaded in the wp media manager
 * requires the ALLOW_UNFILTERED_UPLOADS constant to be set to true in wp-config.php
 * @param  array $mime_types supported mime types
 * @return array $mime_types updated supported mime types
 */
function enable_extended_upload($mime_types = array()){
	$mime_types['json'] = 'application/json';
	$mime_types['msword'] = 'application/msword';
	$mime_types['openofficexml'] = 'application/vnd.openxmlformats-officedocument.wordprocessingml.document';
	$mime_types['openoffice'] = 'application/vnd.oasis.opendocument.text';
	$mime_types['svg_xml'] = 'image/svg+xml';
	$mime_types['svg'] = 'image/svg';

	return $mime_types;
}
add_filter('upload_mimes', 'enable_extended_upload');
