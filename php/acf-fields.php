<?php
if(function_exists('acf_add_local_field_group')){
	$post_id = isset($_GET['post']) ? $_GET['post'] : (isset($_POST['post_ID']) ? $_POST['post_ID'] : null);
	if(!empty($post_id) && isset($post_id)){
		$post_url = urlencode(get_permalink($post_id));
	}
	/**
	* Global fields
	*/
	/**
	 * SEO Options
	*/
	acf_add_local_field_group(array(
		'key' => 'seo_group',
		'title' => __('SEO Options', 'starter_basic_admin'),
		'menu_order' => 99998,
		'position' => 'normal',
		'style' => 'default',
		'label_placement' => 'top',
		'instruction_placement' => 'label',
		'hide_on_screen' => '',
		'active' => true,
		'description' => '',
		'location' => array(
			array(
				array(
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'page',
				),
			),
			array(
				array(
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'post',
				),
			),
			array(
				array(
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'event',
				),
			),
			array(
				array(
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'faq',
				),
			),
			array(
				array(
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'post-archive-options',
				),
			),
		),
		'fields' => array(
			array(
				'key' => 'seo_tab',
				'label' => 'General SEO',
				'name' => 'seo_tab',
				'type' => 'tab',
				'placement' => 'left',
				'endpoint' => 0,
			),
			array(
				'key' => 'seo_instructions',
				'label' => __('SEO Instructions', 'starter_basic_admin'),
				'name' => 'seo_instructions',
				'instructions' => __('If these fields are left blank, SEO information will be generated from the title, content and thumbnail image from the post. They are intended as overrides, so you don\'t need to fill them out. Description is limited to the recommended 160 characters.', 'starter_basic_admin'),
				'type' => 'text',
				'wrapper' => array(
					'width' => '50',
					'class' => 'acf-instructions',
					'id' => 'seo-instructions',
				),
				'maxlength' => '160',
				'rows' => '5',
			),
			array(
				'key' => 'seo_image',
				'label' => __('SEO Image', 'starter_basic_admin'),
				'name' => 'seo_image',
				'type' => 'image',
				'wrapper' => array(
					'width' => '25',
					'class' => '',
					'id' => 'seo-image',
				),
				'preview_size' => 'medium',
			),
			array(
				'key' => 'seo_title',
				'label' => __('SEO Title', 'starter_basic_admin'),
				'name' => 'seo_title',
				'placeholder' => '',
				'type' => 'text',
				'wrapper' => array(
					'width' => '25',
					'class' => '',
					'id' => 'seo-title',
				),
			),
			array(
				'key' => 'seo_description',
				'label' => __('SEO Description', 'starter_basic_admin'),
				'name' => 'seo_description',
				'type' => 'textarea',
				'new_lines' => '',
				'wrapper' => array(
					'width' => '25',
					'class' => '',
					'id' => 'seo-desc',
				),
				'maxlength' => '160',
				'rows' => '5',
			),
			array(
				'key' => 'facebook_tab',
				'label' => 'Facebook Content',
				'name' => 'facebook_tab',
				'type' => 'tab',
				'placement' => 'left',
				'endpoint' => 0,
			),
			array(
				'key' => 'facebook_instructions',
				'label' => __('Facebook Instructions', 'starter_basic_admin'),
				'name' => 'facebook_instructions',
				'instructions' => __('These fields are used for the content shown when this page is shared on Facebook. If these fields are left blank, the information will be generated from the title, content and thumbnail image from the post. They are intended as overrides, so you don\'t need to fill them out. Description is limited to the recommended 160 characters.', 'starter_basic_admin'),
				'type' => 'text',
				'wrapper' => array(
					'width' => '50',
					'class' => 'acf-instructions',
					'id' => 'facebook-instructions',
				),
				'maxlength' => '160',
				'rows' => '5',
			),
			array(
				'key' => 'facebook_image',
				'label' => __('Facebook Image', 'starter_basic_admin'),
				'name' => 'facebook_image',
				'type' => 'image',
				'wrapper' => array(
					'width' => '25',
					'class' => '',
					'id' => 'facebook-image',
				),
				'preview_size' => 'medium',
			),
			array(
				'key' => 'facebook_title',
				'label' => __('Facebook Title', 'starter_basic_admin'),
				'name' => 'facebook_title',
				'placeholder' => '',
				'type' => 'text',
				'wrapper' => array(
					'width' => '25',
					'class' => '',
					'id' => 'facebook-title',
				),
			),
			array(
				'key' => 'facebook_description',
				'label' => __('Facebook Description', 'starter_basic_admin'),
				'name' => 'facebook_description',
				'type' => 'textarea',
				'new_lines' => '',
				'wrapper' => array(
					'width' => '25',
					'class' => '',
					'id' => 'facebook-desc',
				),
				'maxlength' => '160',
				'rows' => '5',
			),
			array(
				'key' => 'twitter_tab',
				'label' => 'Twitter Content',
				'name' => 'twitter_tab',
				'type' => 'tab',
				'placement' => 'left',
				'endpoint' => 0,
			),
			array(
				'key' => 'twitter_instructions',
				'label' => __('Twitter Instructions', 'starter_basic_admin'),
				'name' => 'twitter_instructions',
				'instructions' => __('These fields are used for the content shown when this page is shared on Twitter. If these fields are left blank, the information will be generated from the title, content and thumbnail image from the post. They are intended as overrides, so you don\'t need to fill them out. Description is limited to the recommended 160 characters.', 'starter_basic_admin'),
				'type' => 'text',
				'wrapper' => array(
					'width' => '50',
					'class' => 'acf-instructions',
					'id' => 'twitter-instructions',
				),
				'maxlength' => '160',
				'rows' => '5',
			),
			array(
				'key' => 'twitter_image',
				'label' => __('Twitter Image', 'starter_basic_admin'),
				'name' => 'twitter_image',
				'type' => 'image',
				'wrapper' => array(
					'width' => '25',
					'class' => '',
					'id' => 'twitter-image',
				),
				'preview_size' => 'medium',
			),
			array(
				'key' => 'twitter_title',
				'label' => __('Twitter Title', 'starter_basic_admin'),
				'name' => 'twitter_title',
				'placeholder' => '',
				'type' => 'text',
				'wrapper' => array(
					'width' => '25',
					'class' => '',
					'id' => 'twitter-title',
				),
			),
			array(
				'key' => 'twitter_description',
				'label' => __('Twitter Description', 'starter_basic_admin'),
				'name' => 'twitter_description',
				'type' => 'textarea',
				'new_lines' => '',
				'wrapper' => array(
					'width' => '25',
					'class' => '',
					'id' => 'twitter-desc',
				),
				'maxlength' => '160',
				'rows' => '5',
			),
			array(
				'key' => 'schema_tab',
				'label' => 'Schema.org Content',
				'name' => 'schema_tab',
				'type' => 'tab',
				'placement' => 'left',
				'endpoint' => 0,
			),
			array(
				'key' => 'schema_instructions',
				'label' => __('Schema.org JSON', 'starter_basic_admin'),
				'name' => 'schema_instructions',
				'instructions' => __('Schema.org data, or Google structured data, is used by search engines to determine the specific type of content displayed on each page. For example, the data on a product page will contain information about the product like price range, availability, brand, etc. The data should be entered as a JSON object below. It is important to note that JSON is a very finicky format. Please be sure to check the built in validator in the code editor below for any errors (a red X in the margin). For more information about schema.org data, visit <a href="https://schema.org" target="_blank">schema.org</a>. For more information about the JSON file format, visit <a href="https://www.json.org/json-en.html" target="_blank">json.org</a>. Your data can be tested with <a href="https://search.google.com/test/rich-results?url=' . (isset($post_url) ? $post_url : '') . '" target="_blank">Google\'s Rich Text Testing Tool</a>', 'starter_basic_admin'),
				'type' => 'text',
				'wrapper' => array(
					'width' => '',
					'class' => 'acf-instructions',
					'id' => 'schema-instructions',
				),
				'maxlength' => '160',
				'rows' => '5',
			),
			array(
				'key' => 'schema_content',
				'label' => __('Schema.org content', 'starter_basic_admin'),
				'name' => 'schema_content',
				'type' => 'textarea',
				'new_lines' => '',
				'wrapper' => array(
					'width' => '',
					'class' => '',
					'id' => 'schema-json',
				),
				'maxlength' => '',
				'rows' => '',
			),
		),
	));

	acf_add_local_field_group(array(
		'key' => 'arbitrary_code',
		'title' => __('Custom Code', 'starter_basic_admin'),
		'menu_order' => 99999,
		'position' => 'normal',
		'style' => 'default',
		'label_placement' => 'top',
		'instruction_placement' => 'label',
		'hide_on_screen' => '',
		'active' => true,
		'description' => '',
		'location' => array(
			array(
				array(
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'page',
				),
			),
			array(
				array(
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'post',
				),
			),
			array(
				array(
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'event',
				),
			),
			array(
				array(
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'faq',
				),
			),
			array(
				array(
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'post-archive-options',
				),
			),
		),
		'fields' => array(
			array(
				'key' => 'arbitrary_code_head',
				'label' => __('In the <code>&lt;head&gt;</code> tag', 'starter_basic_admin'),
				'name' => 'arbitrary_code_head',
				'type' => 'tab',
				'placement' => 'left',
				'endpoint' => 0,
			),
			array(
				'key' => 'arbitrary_code_head_instructions',
				'label' => __('Instructions', 'starter_basic_admin'),
				'name' => 'arbitrary_code_head_instructions',
				'instructions' => __('This is instructions', 'starter_basic_admin'),
				'type' => 'text',
				'wrapper' => array(
					'width' => '50',
					'class' => 'acf-instructions',
					'id' => 'arbitrary-code-head-instructions',
				),
				'maxlength' => '160',
				'rows' => '5',
			),
			array(
				'key' => 'arbitrary_code_head_content',
				'label' => __('HTML Content', 'starter_basic_admin'),
				'name' => 'arbitrary_code_head_content',
				'type' => 'textarea',
				'new_lines' => '',
				'wrapper' => array(
					'width' => '',
					'class' => '',
					'id' => 'arbitrary-code-head-content',
				),
				'maxlength' => '',
				'rows' => '',
			),
			array(
				'key' => 'arbitrary_code_begin_body',
				'label' => 'After the opening <code>&lt;body&gt;</code> tag',
				'name' => 'arbitrary_code_begin_body',
				'type' => 'tab',
				'placement' => 'left',
				'endpoint' => 0,
			),
			array(
				'key' => 'arbitrary_code_begin_body_instructions',
				'label' => __('Instructions', 'starter_basic_admin'),
				'name' => 'arbitrary_code_begin_body_instructions',
				'instructions' => __('This is instructions', 'starter_basic_admin'),
				'type' => 'text',
				'wrapper' => array(
					'width' => '50',
					'class' => 'acf-instructions',
					'id' => 'arbitrary-code-begin-body-instructions',
				),
				'maxlength' => '160',
				'rows' => '5',
			),
			array(
				'key' => 'arbitrary_code_begin_body_content',
				'label' => __('HTML Content', 'starter_basic_admin'),
				'name' => 'arbitrary_code_begin_body_content',
				'type' => 'textarea',
				'new_lines' => '',
				'wrapper' => array(
					'width' => '',
					'class' => '',
					'id' => 'arbitrary-code-begin-body-content',
				),
				'maxlength' => '',
				'rows' => '',
			),
			array(
				'key' => 'arbitrary_code_end_body',
				'label' => 'Before the closing <code>&lt;body&gt;</code> tag',
				'name' => 'arbitrary_code_end_body',
				'type' => 'tab',
				'placement' => 'left',
				'endpoint' => 0,
			),
			array(
				'key' => 'arbitrary_code_end_body_instructions',
				'label' => __('Instructions', 'starter_basic_admin'),
				'name' => 'arbitrary_code_end_body_instructions',
				'instructions' => __('This is instructions', 'starter_basic_admin'),
				'type' => 'text',
				'wrapper' => array(
					'width' => '50',
					'class' => 'acf-instructions',
					'id' => 'arbitrary-code-end-body-instructions',
				),
				'maxlength' => '160',
				'rows' => '5',
			),
			array(
				'key' => 'arbitrary_code_end_body_content',
				'label' => __('HTML Content', 'starter_basic_admin'),
				'name' => 'arbitrary_code_end_body_content',
				'type' => 'textarea',
				'new_lines' => '',
				'wrapper' => array(
					'width' => '',
					'class' => '',
					'id' => 'arbitrary-code-end-body-content',
				),
				'maxlength' => '',
				'rows' => '',
			),
		),
	));
	foreach (glob(__DIR__ . '/acf-views/*.php') as $acf_view){
		include $acf_view;
	}

	foreach (glob(__DIR__ . '/acf-blocks/*.php') as $acf_view){
		include $acf_view;
	}
}
