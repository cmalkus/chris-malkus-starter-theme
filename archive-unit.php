<?php
/**
 * The Template for displaying all single posts
 *
 * Methods for TimberHelper can be found in the /lib sub-directory
 *
 * @package  WordPress
 * @subpackage  Timber
 * @since    Timber 0.1
 */

global $wp, $wpdb, $unit_query, $post;

$units_raw = $wpdb->get_results($unit_query);
$units = group_by_object_key($units_raw, 'viewPlaneDirection');
$bed_bath = array();
$floors = array();

foreach($units as $direction => &$value){
	$units[$direction] = group_by_object_key(array_reverse($value), 'level');

	foreach($value as $floor => &$obj){
		if(!in_array($floor, $floors)){
			$floors[] = $floor;
		}

		uasort($obj, function($a, $b) use(&$bed_bath){
			$tmp = new stdClass();

			$tmp->bedrooms = $a->bedrooms;
			$tmp->bathrooms = $a->bathrooms;

			if(!in_array($tmp, $bed_bath) && $a->available == 1){
				$bed_bath[] = $tmp;
			}

			if($a->unitNumber == $b->unitNumber){
				return 0;
			}
			return ($a->unitNumber > $b->unitNumber) ? -1 : 1;
		});
	}
}

uasort($bed_bath, function($a, $b){
	if($a->bedrooms == $b->bedrooms){
		if($a->bathrooms == $b->bathrooms){
			return 0;
		}

		return ($a->bathrooms > $b->bathrooms) ? -1 : 1;
	}

	return ($a->bedrooms > $b->bedrooms) ? -1 : 1;
});

uasort($floors, function($a, $b){
	if($a == $b){
		return 0;
	}

	return ($a > $b) ? 1 : -1;
});

$context = Timber::context();

$context['unit_manager'] = new Units\Units;
$context['sidebar'] = false;
$context['title'] = __('All Units', 'starter_basic');
$context['units'] = $units;
$context['bed_bath'] = $bed_bath;
$context['floors'] = $floors;
$context['post'] = new stdClass();
$context['post']->post_content = get_theme_mod('all_unit_info');
$context['post']->title = $context['title'];
$context['post']->link = home_url(add_query_arg(array(), $wp->request));
$context['post']->type = 'page';

Timber::render('archive-unit.twig', $context);
