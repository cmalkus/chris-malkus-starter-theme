<?php
// die(phpinfo());

function mail_error( $wp_error ) {
	error_log(print_r($wp_error, 1));
}
add_action( 'wp_mail_failed', 'mail_error', 10, 1 );

/**
 * This is an array of post types passed to the sitemapper. As you add new post types, list them here if you want them to be sitemapped.
 * @var array
 */

$sitemap_posts = array('post', 'page', 'event', 'faq');
$robots_disallow = array('user', 'form', 'documentation');
$unit_query = "SELECT * FROM wp_building_units";
/**
 * An array of AFC field names with content you want to be searchable with wp search. Data type Strings.
 * @var array
 */
$custom_fields = array('');

if(!defined('TEMPLATEPATH')){
	define('TEMPLATEPATH', get_template_directory());
}

/**
 * Require the built-in Timber plugin, server-side mobile detection, the XML sitemap generator, and the php used for new admin functionality.
 * Mobile detect usage:
 * $var = new Mobile_Detect();
 * $var->isMobile(), $var->isTablet(), etc. See documentation for full features.
 *	 TODO Find a better way to include the plugin. Currently the plugin will not show up in the WP admin
 */
require_once get_template_directory() . '/plugins/timber-library/timber.php';
require_once get_template_directory() . '/plugins/advanced-custom-fields-pro/acf.php';
// require_once get_template_directory() . '/plugins/wp-nested-pages/nestedpages.php';

require_once get_template_directory() . '/php/Auth.php';
require_once get_template_directory() . '/php/Breadcrumbs.php';
require_once get_template_directory() . '/php/Block_Utils.php';
require_once get_template_directory() . '/php/Customize_Theme.php';
require_once get_template_directory() . '/php/Image_Utils.php';
require_once get_template_directory() . '/php/Mobile_Detect.php';
require_once get_template_directory() . '/php/Page_Mapping.php';
require_once get_template_directory() . '/php/Sitemap_XML.php';
require_once get_template_directory() . '/php/Sort_Posts.php';
require_once get_template_directory() . '/php/timber-utils.php';

require_once get_template_directory() . '/php/custom-posts/Post_Archive_Options.php';
require_once get_template_directory() . '/php/custom-posts/Documentation.php';
require_once get_template_directory() . '/php/custom-posts/Events.php';
require_once get_template_directory() . '/php/custom-posts/Faqs.php';
require_once get_template_directory() . '/php/custom-posts/Form_Builder.php';
require_once get_template_directory() . '/php/custom-posts/Members.php';
// require_once get_template_directory() . '/php/custom-posts/Units.php';

require_once get_template_directory() . '/php/admin.php';
require_once get_template_directory() . '/php/acf-fields.php';
require_once get_template_directory() . '/php/acf-search.php';

add_filter('wp_headers', function($headers, $wp_query){
	$headers['X-Frame-Options'] = 'SAMEORIGIN';

	return $headers;
}, 11, 2);

add_filter('bloginfo_url', function($output, $property){
	if($property == 'pingback_url') $output = '';

	return $output;
}, 11, 2);

add_action('wp', function(){
	remove_action('wp_head', 'rsd_link');
	if (function_exists('header_remove')) {
		header_remove('x-powered-by');
		header_remove('x-pingback');
	}
}, 11);

add_filter('xmlrpc_enabled', '__return_false');

/**
 * Set up the AFC path to be our built in plugin path
 * @param  string $path path to be overwritten
 * @return string       The full path of the new path... yes that makes sense
 */
function my_acf_settings_path($path){
	$path = get_template_directory() . '/plugins/advanced-custom-fields-pro/';

	return $path;
}
add_filter('acf/settings/path', 'my_acf_settings_path');

/**
 * Set up the AFC dir to be our built in plugin dir
 * @param  string $dir dir to be overwritten
 * @return string       The full path of the new dir
 */
function my_acf_settings_dir($dir){
	$dir = get_template_directory_uri() . '/plugins/advanced-custom-fields-pro/';

	return $dir;
}
add_filter('acf/settings/dir', 'my_acf_settings_dir');

/* uncomment this to hide ACF from the wp-admin */
//add_filter('acf/settings/show_admin', '__return_false');

/**
 * Basic theme stuff here. Curretly just adds thumbnail support to posts
 */
function starter_basic_setup(){
	add_theme_support('post-thumbnails');
	add_theme_support('post-formats', array('aside', 'gallery', 'link', 'image', 'quote', 'status', 'video', 'audio', 'chat'));
}
add_action('after_setup_theme', 'starter_basic_setup');

/**
 * Load text domain for i18n translations
 */
function starter_basic_languages(){
	$mo_path = get_template_directory() . '/languages/';
	if(file_exists($mo_path . get_locale() . '.mo')){
		load_textdomain('starter_basic', $mo_path . get_locale() . '.mo');
	}
}
add_action('after_setup_theme', 'starter_basic_languages');

/**
 * Sets the name of the php template used for the current page to $GLOBALS. Used to load page-specific styles and scripts
 * @param string $template php template name, ending in .php
 * @return string the template name, unmodified
 */
function var_template_include($template){
	$GLOBALS['current_theme_template'] = basename($template);
	return $template;
}
add_filter('template_include', 'var_template_include');

/**
 * Load scripts, but first get rid of the built-in WP version of jQuery.
 * Our concatinated, minified script includes jQuery and any other 3rd party scripts you wish to include
 * Google maps with our API key is included. Can be used for dev, but probably should be changed to a client account for prod.
 */
function starter_basic_load_scripts(){
	$version = wp_get_theme()->get('Version');

	wp_deregister_script('jquery');
	if(!empty(get_theme_mod('google_maps_key'))){
		wp_enqueue_script('google_maps', 'https://maps.googleapis.com/maps/api/js?key=' . get_theme_mod('google_maps_key'), false, '1.0', true);
	}
	wp_enqueue_script('main_js', get_template_directory_uri() . '/js/main.min.js', false, $version, true);

	if(file_exists(get_template_directory() . '/js/app/views/' . pathinfo($GLOBALS['current_theme_template'], PATHINFO_FILENAME) . '.js')){
		wp_enqueue_script('page_js', get_template_directory_uri() . '/js/app/views/' . pathinfo($GLOBALS['current_theme_template'], PATHINFO_FILENAME) . '.js', 'main_js', $version, true);
	}
}
add_action('wp_enqueue_scripts', 'starter_basic_load_scripts');

/**
 * Load our styles. Lets keep this minimal.
 */
function starter_basic_load_styles(){
	$version = wp_get_theme()->get('Version');

	wp_enqueue_style('main_style', get_template_directory_uri() . '/style.css', false, $version, 'screen');
	wp_style_add_data('main_style', 'rtl', 'replace');
	global $wp_styles;

	if(file_exists(get_template_directory() . '/css/views/' . pathinfo($GLOBALS['current_theme_template'], PATHINFO_FILENAME) . '.css')){
		wp_enqueue_style('page_style', get_template_directory_uri() . '/css/views/' . pathinfo($GLOBALS['current_theme_template'], PATHINFO_FILENAME) . '.css', false, $version, 'screen');
		$wp_styles->add_data('page_style', 'disabled', TRUE);
	}
	$wp_styles->add_data('main_style', 'disabled', TRUE);
}
add_action('wp_enqueue_scripts', 'starter_basic_load_styles');

/**
 * Turn off the stupid wordpress emojis. They wouldnt be so bad but they grab all your HTML entities and replace them with img tags. Lame city.
 */
function disable_wp_emojicons(){
	remove_action('print_scripts', 'print_emoji_detection_script');
	remove_action('wp_print_styles', 'print_emoji_styles');
	remove_action('admin_print_scripts', 'print_emoji_detection_script');
	remove_action('admin_print_styles', 'print_emoji_styles');
	remove_action('wp_head', 'print_emoji_detection_script', 7);
	remove_filter('wp_mail', 'wp_staticize_emoji_for_email');
	remove_filter('the_content_feed', 'wp_staticize_emoji');
	remove_filter('comment_text_rss', 'wp_staticize_emoji');
}
add_action('init', 'disable_wp_emojicons');
add_action('admin_init', 'disable_wp_emojicons');

/**
 * Create custom image sizes.
 * Use add_image_size( 'thumbnail_size_name', width, height, array( 'horiz_scale_origin', 'vert_scale_origin' ) );
 */
function starter_basic_custom_image_sizes(){
	add_image_size('fullscreen_slider', 1920, 600, array('center', 'center'));
	add_image_size('mobile_fullscreen_slider', 600, 187, array('center', 'center'));
	add_image_size('lazy_fullscreen_slider', 100, 31.25, array('center', 'center'));
	add_image_size('lazy_orginal_ratio', 100, -9999, false);
}
add_action('after_setup_theme', 'starter_basic_custom_image_sizes');

/**
 * Removes the admin bar from the front end for chump users with no role.
 */
function remove_admin_bar(){
	if(wp_get_current_user()->roles && wp_get_current_user()?->roles[0] == ''){
		show_admin_bar(false);
	}
}
add_action('after_setup_theme', 'remove_admin_bar');

/**
 * Disabling the Gutenberg editor all post types except post.
 *
 * @param bool   $flag  Whether to use the Gutenberg editor.
 * @param string $post	the wp post.
 * @return bool
 */
function disable_gutenberg($flag, $post){
	$id_disable = array(

	);
	$type_disable = array();

	if(in_array($post->ID, $id_disable)){
		$flag = false;
	}

	if(in_array($post->post_type, $type_disable)) {
		$flag = false;
	}

	return $flag;
}
add_filter('use_block_editor_for_post', 'disable_gutenberg', 10, 2);

function hide_editor(){
	$post_id = isset($_GET['post']) ? $_GET['post'] : (isset($_POST['post_ID']) ? $_POST['post_ID'] : null);

	$id_disable = array(

	);

	$template_disable = array(

	);

	if(!isset($post_id)) return;

	if(in_array($post_id, $id_disable)){
		remove_post_type_support('page', 'editor');
	}

	if(in_array(get_page_template_slug($post_id), $template_disable)){
		remove_post_type_support('page', 'editor');
	}
}
add_action('admin_init', 'hide_editor');

/**
 * Adds custom messages to the WP admin
 * @param  string $msg The message you want to display
 * @param  string $lvl The level (or color) of the notice. Valid vals are 'notice-error', 'notice-warning', 'notice-success', and 'notice-info'
 */
function starter_basic_notice($msg = 'no message', $lvl = 'notice-info'){
	?>
	<div class="notice <?php echo $lvl; ?>">
		<p><?php echo $msg; ?></p>
	</div>
<?php
}

/**
 * Gets the path of the user's private upload directory. Located in uploads, its their nicename
 * @param  stdObj $user wordpress user
 * @return str       the user's upload path
 */
function get_user_dir($user){
	$upload_dir = wp_upload_dir();
	$subdir = $user->user_nicename;
	$modified['subdir'] = $subdir;
	$modified['url'] = $upload_dir['baseurl'] . '/' . $subdir;
	$modified['path'] = $upload_dir['basedir'] . DIRECTORY_SEPARATOR . $subdir;

	return $modified;
}

/**
 * Function that groups an array of associative arrays by some key.
 *
 * @param String $key Property to sort by.
 * @param Array $data Array that stores multiple associative arrays.
 */
function group_by_array_key($array, $key){
	$grouped_array = array();

	foreach($array as $item){
		$grouped_array[$item[$key]][] = $item;
	}

	return $grouped_array;
}

/**
 * group an array of objects by a key
 * @param  array  $array array to be grouped
 * @param  string $key   object key to group by
 * @return array         grouped array
 */
function group_by_object_key($array, $key){
	$grouped_array = array();

	foreach($array as $item){
		$grouped_array[$item->$key][] = $item;
	}

	return $grouped_array;
}

/**
 * filter an array of objects by key/value
 * @param  array  $array array to be filtered
 * @param  string $key   object key to look at
 * @param  string $value value of key to filter by
 * @return array         filtered array
 */
function filter_by_object_value($array, $key, $value){
	$filtered_array = array();

	foreach($array as $item){
		if (isset($item->$key)){
			if(strtolower($item->$key) == strtolower($value)){
				$filtered_array[] = $item;
			}
		}
	}

	return $filtered_array;
}

/**
 * A simple post sorter.
 * @param  array   $posts   An array of the posts you need to sort
 * @param  string  $orderby The field you want to or by
 * @param  string  $order   ASC or DESC
 * @param  boolean $unique  When true, the sort will also remove duplicate post
 * @return array            Sorted list of posts
 */
function sort_posts($posts, $orderby, $order = 'ASC', $unique = true){
	if(!is_array($posts)){
		return false;
	}

	usort($posts, array(new Sort_Posts($orderby, $order), 'sort'));

	// use post ids as the array keys
	if($unique && count($posts)){
		$posts = array_combine(wp_list_pluck($posts, 'ID'), $posts);
	}

	return $posts;
}

/**
 * Adds menu locations to the menu manager
 */
function make_menus(){
	register_nav_menus(array(
		'main_navigation' => __('Main Navigation', 'starter_basic_admin'),
		'footer_navigation' => __('Footer Navigation', 'starter_basic_admin'),
		'sitemap' => __('Sitemap', 'starter_basic_admin'),
	));
}
add_action('after_setup_theme', 'make_menus');

function custom_media_add_media_custom_field( $form_fields, $post ) {
	$x_origin = get_post_meta( $post->ID, 'crop_origin_x', true );
	$y_origin = get_post_meta( $post->ID, 'crop_origin_y', true );

	$form_fields['crop_origin_x'] = array(
		'value' => $x_origin ? $x_origin : '.5',
		'input'  => 'hidden'
	);

	$form_fields['crop_origin_y'] = array(
		'value' => $y_origin ? $y_origin : '.5',
		'input'  => 'hidden'
	);

	ob_start(); ?>
	<script>
		(function($){
			var image = $('.details-image'),
				xInput = $('[name="attachments[<?php echo $post->ID; ?>][crop_origin_x]"]'),
				yInput = $('[name="attachments[<?php echo $post->ID; ?>][crop_origin_y]"]');

			if(!image.parent().is('.origin-wrapper')){
				$('.details-image').wrap('<div class="origin-wrapper"></div>');
			}

			if(image.siblings('.crop-origin').length < 1){
				$('<span class="crop-origin"></span>').appendTo('.origin-wrapper');
			}

			image.load(function(){
				$('.crop-origin').css({'top': yInput.val() * image.height() - 12, 'left': xInput.val() * image.width() + parseInt(image.css('margin-left')) - 12});
			});

			image.click(function(e){
				var that = $(this),
					posX = (e.pageX - that.offset().left) / that.width(),
					posY = (e.pageY - that.offset().top) / that.height();

				xInput.val(posX);
				yInput.val(posY);
				xInput.change();
				yInput.change();

				$('.crop-origin').css({'top': posY * image.height() - 12, 'left': posX * image.width() + parseInt(image.css('margin-left')) - 12});
			});
		})(jQuery);
	</script> <?php

	$image_coords = ob_get_clean();

	$form_fields['image_coords'] = array(
		'tr' => $image_coords,
	);

	return $form_fields;
}
add_filter( 'attachment_fields_to_edit', 'custom_media_add_media_custom_field', null, 2 );

function custom_media_save_attachment( $attachment_id ) {
	if (isset($_REQUEST['attachments'][ $attachment_id ]['crop_origin_x']) && isset($_REQUEST['attachments'][ $attachment_id ]['crop_origin_y'])) {
		$x_origin = $_REQUEST['attachments'][ $attachment_id ]['crop_origin_x'];
		$y_origin = $_REQUEST['attachments'][ $attachment_id ]['crop_origin_y'];

		update_post_meta($attachment_id, 'crop_origin_x', $x_origin);
		update_post_meta($attachment_id, 'crop_origin_y', $y_origin);
	}
}
add_action('edit_attachment', 'custom_media_save_attachment');

/**
 * Adds widget locations to the widget manager
 */
function starter_basic_widgets_init(){
	register_sidebar(array(
		'name' => __('Sidebar Widget Area', 'starter_basic_admin'),
		'id' => 'sidebar-widget-area',
		'before_widget' => '<section id="%1$s" class="%2$s">',
		'after_widget' => '</section>',
		'before_title' => '<h5>',
		'after_title' => '</h5>',
	));
}
add_action('widgets_init', 'starter_basic_widgets_init');

/**
 * redirects to /search instead of using url params
 */
function starter_basic_search_url(){
	if(is_search() && !empty($_GET['s'])){
		wp_redirect(home_url('/search/') . urlencode(get_query_var('s')));
		exit();
	}

	if(preg_match('/\/site-map((\/\w+)+|\/?)$/', $_SERVER['REQUEST_URI'])){
		status_header(200);
		$GLOBALS['current_theme_template'] = basename('site-map.php');
		require_once get_template_directory() . '/site-map.php';

		exit();
	}
}
add_action('template_redirect', 'starter_basic_search_url');


function nav_class($classes, $item, $args){
	error_log(print_r($classes, 1));

	return [];
}
add_filter( 'nav_menu_css_class' , 'nav_class' , 0, 4 );